#include "server.h"
#include <cstring>

// When the application starts, ensure that the -sid arg is passed along with an integer
// this int is used in determining the base server ID for retrieving configs, executing ASes and other things
void  __attribute__((constructor)) init (int argc, char** argv)
{
//	fclose(stderr);
	unsigned int serverId = 0;
	
	for(unsigned int i=0;i<argc;i++)
	{
		if(strcmp(argv[i], "-sid") == 0)
		{
			serverId = atoi(argv[i + 1]);
		}
	}
	
	if(serverId == 0)
	{
		printf("Server ID not found, now exiting. Use -sid <serverid> in your command stack to initialize a server ID\nAborting\n");
		exit(0);
	}
	
	// Initialize the server hooks and prep objects	
    Server::Init(serverId);
}


