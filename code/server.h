/**
 
[ 0]            0x840ccd0, Test, [string]
[ 1]            0x84c00f8, Test, [string]
[ 2]            0x84c0168, Test, [string] // This one when changed it changes the player's name (Looks like the player's array starts @ 0x084C008)
[ 3]            0x887c381, Test, [string]
[ 4]            0x8930862, Test, [string]
[ 5]            0x8c10f87, Test, [string]
[ 6]            0x90b5634, Test, [string]
[ 7]            0x90d5e7a, Test, [string]
[ 8]            0x90d621c, Test, [string]

 * 
 * First player's name: 0x084c0168 [Array starts @ 0x084C0088]
 * Second player's name: 0x084c32ec [Array starts @ 0x084C320C]
 
 * Function that changes a few of the player's parameters including name @ unsigned int 0x081703D6(unsigned long playerPointer)
 */

/**
 * There is an array of all the cvars @ 1402C060 (This contains the array's length)
 * 
 * Array starts @ 1402C080 and is 76 bytes long.
 * First 4 bytes contain the char* to the variable's name
 * Second 4 bytes contained the char* to the variable's description
 * 
 * 
 * There is a function in which can create a cvar 
 *		(String @ 081A2944 (char* varName, char* defaultValue, ?unsigned long maxLen?, char* varDesc))
 *		(String @ 081A2CC6 (char* varName, char* defaultValue, unsigned long min, unsigned long max, ?unsigned long maxLen?, char* varDesc))
 */

#ifndef SERVER_H
#define SERVER_H

#include <vector>
#include "player.h"
#include "objects/playergroup.h"

#include "asengine/engine.h"

using namespace std;

//class Engine;
class Hook;
//class Ban;
class Database;

class Server {
	// Version information
	static const unsigned int versionMajor = 1;
	static const unsigned int versionMinor = 4;
	static const unsigned int versionPatch = 0;
	
	// Data information
	static const unsigned int PLAYERNAME_MAX_SIZE = 14;
	
	// Virtual function types
	typedef int          (*mServerInitFuncType)(u_int32_t a1);
	typedef int          (*mServerCommonInit)(u_int32_t a1);
	typedef bool         (*mPlayerJoinRequest)(u_int32_t a1, void* a2, u_int32_t a3, u_int32_t a4, void* a5);
	typedef int          (*mPlayerDisconnect)(unsigned long playerOffset, void* a2, unsigned int reason);
	typedef bool         (*mPacketResponse)(signed int a1, unsigned long a2, void* a3, unsigned long a4, unsigned long a5, unsigned long a6, const char *a7);
	typedef int          (*mSendServerCmdFuncType)(void* clientPtr, signed int a2, const char *a3, ...);
	typedef void*        (*mComBuffFuncType)(int a1, const char* a2, ...);
	typedef int          (*mPlayerSayFuncType)(int a1, int a2, int a3, int a4);
	typedef char*        (*mAdrToStringFuncType)(unsigned long a1, void* a2, unsigned long a3, unsigned long a4, void* a5);
	typedef int          (*mExecCmd)(unsigned long a1, unsigned long a2, const char* command);
	typedef int          (*mPlayerNameChange)(u_int32_t playerPointer);
	typedef unsigned int (*mPlayerForceUserinfo)(unsigned long playerPointer);
	typedef unsigned int (*mPlayerForceName)(unsigned long playerId);
	typedef char*        (*mGetValueFromSlashString)(char* slashedString, char* key);
	typedef void*        (*mGetPlayerBuffer)(unsigned int playerId, char* dst, unsigned int size);
	
	typedef const char*   (*mEnsureAppendage)(const char *filename, unsigned int a2, const char* append);
	typedef const char*   (*mReadCfg)(void* a1, unsigned long* a2, void* a3);
	typedef unsigned long (*mCvarCreateInt)(char* varName, unsigned long defaultValue, unsigned long min, unsigned long max, unsigned long maxLen, char* varDesc);
	
	

	// Virtual function locations
	static const unsigned long smServerInitLoc           = 0x0817D2D8; // Hooked
	static const unsigned long smServerCommonInitLoc     = 0x08123080; // Hooked
	static const unsigned long smPlayerJoinRequestLoc    = 0x0817153E; // Hooked
	static const unsigned long smPlayerDisconnectLoc     = 0x08170A26; // Hooked
	static const unsigned long smPacketResponseLoc       = 0x0813D086; // Hooked
	static const unsigned long smRconStatusLoc           = 0x0816C708; // Hooked
	static const unsigned long smSendServerCmdLoc        = 0x08177402; // Callable
	static const unsigned long smComBuffLoc              = 0x08122B2E; // Callable
	static const unsigned long smPlayerSayLoc            = 0x080AE962; // Hooked
	static const unsigned long smAdrToStringLoc          = 0x0813BB30; // Callable
	static const unsigned long smExecCmdLoc              = 0x08111BEA;
	static const unsigned long smPlayerNameChangeLoc     = 0x081705EC; // Hooked
	static const unsigned long smPlayerForceUserinfoLoc  = 0x081703D6;
	static const unsigned long smPlayerForceNameLoc      = 0x080A8068;
	static const unsigned long smGetValueFromSlashString = 0x081AAB50;
	static const unsigned long smGetPlayerBufferLoc      = 0x08173E5C;
	
	//	static const unsigned long smEvalCmdLoc = 0x08110FF8; // Callable
	static const unsigned long smEnsureAppendage = 0x081AAE08; // Callable/Hookable
	static const unsigned long smReadCfgLoc      = 0x0818BB8C;
	static const unsigned long smCvarCreateInt   = 0x081A2CC6;
	
	// Virtual variable locations
	static const unsigned long smMaxClientsLoc = 0x8370624; // Max clients allowed on the server
	
	// Data structures
	struct Limbo {
		char*  ip;
		long   uniq;
		string denyReason;
		int    state = 0; // 0 - Limbo, 1 - Accept, 2 - Deny
	};
	struct ThreadPlayerLoginArg {
		u_int32_t playerId;
		string    username;
		string    password;
	};

	public:
	/** Internalized stuff */
		
		// Functions
		static void Init(unsigned int serverId);
		static void RegisterHooks();
		
		static unsigned int GetMaxClients();
		static PlayerGroup* GetGroupById(unsigned int id);
		static int          ExecCmd(std::string cmd);
		static void         ExecConfig(std::string cfg);
		static Player*      GetPlayerByOffset(u_int32_t offset);
		static u_int32_t    GetPlayerIdByOffset(u_int32_t offset);
		
		static void PrintBanner();
		
		// Threads
		static void* ThreadPlayerLogin(void* _arg);
		
		// Debug printing
		static void PrintDebug(std::string message);
		static void PrintLimboDebug(std::string message);
		static void PrintFatal(std::string message);
		static void PrintNoEventFound(std::string message);
		
		// Vars
		static char          aliceVersion[21];
		static unsigned int  communityId;
		static unsigned int  serverId;
		static unsigned int* maxClients; // Pointer to server variable
		
		// Sigil set variables
		static char*         cfgChunk;      // Config
		static vector<char*> cfgChunkLines; // Config split into lines
		static unsigned int  cfgPort;
		static unsigned int  cfgMaxClients;
		static char*         cfgServerName;
		
		static ASEngine::Engine* asEngine;
		static Database*         database;
		
		static vector<Player*>         players;
		static vector<PlayerGroup*>    groups;
		static vector<Limbo> limboConnections;
		
	/** =============== */


	/** ALICE SCRIPT */
		
		static void LogASNoEvent(const char* funcName);
		
		// System Functions
		
		static char* ASGetAliceVersion();
		static void  ASSleep(unsigned long msec);
		static void  ASGetMaxClients(const v8::FunctionCallbackInfo<v8::Value>& info);
		static void  ASSendMessageToAll(const v8::FunctionCallbackInfo<v8::Value>& args);
		static void  ASInclude(const v8::FunctionCallbackInfo<v8::Value>& args);
		
		// Player Functions
		
		static void ASGetPlayerById(const v8::FunctionCallbackInfo<v8::Value>& info);
//		static Sqrat::Array ASFindPlayersByName(char* partialName);
//		static Sqrat::Array ASFindPlayersByIp(char* partialIp);
		
		// Limbo functions
		
		static void ASLimboAccept(char* ip, bool enforce);
		static void ASLimboDeny(char* ip, char* reason, bool enforce);

		static void ASDelegateGroups();

		// Offence/Violation Functions

//		static Ban* ASLoadBan(char* ip);
//		static Ban  ASGetBanByIp(char* ip);
		
		// Gameplay Functions
		
		static void ASLoadNextMap();
		static void ASRestartMap();
		static void ASResetMap();
		static void ASLoadMap(std::string mapName);
		
		// Bound Classes

		
	/** =============== */
		
		
	/** PATCHES */
		
		static const char*   HEnsureAppendage(const char *filename, unsigned int a2, const char* append);
		static const char*   HReadCfg(void* a1, unsigned long* a2, void* a3);
		static unsigned long HCvarCreateInt(char* varName, unsigned long defaultValue, unsigned long min, unsigned long max, unsigned long maxLen, char* varDesc);
		
		static Hook* hEnsureAppendage;
		static Hook* hReadCfg;
		static Hook* hCvarCreateInt;
		
	/** =============== */
		
		
	/** HOOKED EVENTS */
		
		// Events
		static int  HServerCommonInit(int a1);
		static int  HServerInit(int a1);
		static bool HPlayerJoinRequest(unsigned long a1, void* a2, unsigned long a3, unsigned long a4, void* a5);
		static bool HPlayerDisconnect(unsigned long playerOffset, void* a2, unsigned int reason);
		static bool HPacketResponse(signed int a1, int a2, int a3, int a4, char a5, char a6, const char *a7);
		static int  HPlayerSay(unsigned int a1, int a2, int a3, int a4);
		static void HServerStatusRequest();
		static int  HPlayerNameChange(u_int32_t playerOffset);
		
		// Hooks
		static Hook* hServerCommonInit;
		static Hook* hServerInit;
		static Hook* hPlayerJoinRequest;
		static Hook* hPlayerDisconnect;
		static Hook* hPacketResponse;
		static Hook* hPlayerSay;
		static Hook* hServerStatusRequest;
		static Hook* hPlayerNameChange;
		
		// Threads
		static void* HTServerStatusRequest(void* threadId);
		static void* HTPlayerJoinRequest(char* playerAdr);
		
		// Parameters
		struct HPlayerJoinRequestParam{ u_int32_t a1; void* a2; u_int32_t a3; u_int32_t a4; void* a5; };
		
	/** =============== */
		
		
		

	protected:
	private:
		
		

};

#endif // SERVER_H