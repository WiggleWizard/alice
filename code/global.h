/* 
 * File:   global.h
 * Author: terence
 *
 * Created on 11 March 2014, 09:41
 */

#ifndef GLOBAL_H
#define	GLOBAL_H

/* CONSOLE COLORS */
#define CONNRM  "\x1B[0m"  // Reset
#define CONRED  "\x1b[31m" // Red
#define CONGRN  "\x1B[32m" // Green
#define CONYEL  "\x1B[33m" // Yellow
#define CONBLU  "\x1B[34m" // Blue
#define CONMAG  "\x1B[35m" // Magenta
#define CONCYN  "\x1B[36m" // Cyan
#define CONRST  "\x1B[30m" // White

#endif	/* GLOBAL_H */

