/* 
 * File:   asevent.h
 * Author: terence
 *
 * Created on 27 February 2014, 16:57
 */

#ifndef ASEVENT_H
#define	ASEVENT_H

namespace ASEngine {
	
/**
 * AliceScript spec'd wrapper for Sqrat/Squirrel
 */
class Event {
		
	public:
		
	private:
		
};

/**
 * AliceScript spec'd wrapper for Sqrat/Squirrel which leverages
 * pthreads to trigger/execute events
 */
class ThreadedEvent {

	public:
		
		
		
	private:
		
		
};
}

#endif	/* ASEVENT_H */

