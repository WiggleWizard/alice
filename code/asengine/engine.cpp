#include <stdarg.h>
#include <limits.h>
#include <v8.h>

#include "../global.h"

#include "engine.h"
#include "../player.h"
//#include "../objects/ban.h"
#include "../server.h"
#include "../objects/playergroup.h"
#include "../utils.h"

#include <algorithm>    // For removing leading tabs from error message

using namespace v8;

/**
 * Constructs the VM and does initial constructions
 */
ASEngine::Engine::Engine()
{
	printf("[AliceScript]-[Engine] Constructing AS V8 engine\n");

	V8::Initialize();
	
	/* Create isolate */
	this->isolate = Isolate::GetCurrent();
	HandleScope scope(this->isolate);
	
	/* Register empty object template holder */
	this->global = ObjectTemplate::New(this->isolate);
	this->RegisterGlobalBinds();
	
	// Persist the context
	this->context.Reset(this->isolate, Context::New(this->isolate, NULL, global));
	
	printf("[AliceScript]-[Engine] Done constructing engine\n");
}



// ** 1. Engine Helpers **************************************************//
// =======================================================================//

	/**
	 * Resets the engine and clears out cached functions
     * @return 
     */
	bool ASEngine::Engine::Reset()
	{
		printf("[AliceScript]-[Engine] Resetting AS V8 engine\n");
		
		// Kill the isolate and threads attached to it
		this->isolate->Exit();
		
		/* Reinit the engine */
		V8::Initialize();
		this->isolate = Isolate::GetCurrent();
		HandleScope scope(this->isolate);
		
		/* Register empty object template holder */
		this->global = ObjectTemplate::New(this->isolate);
		this->RegisterGlobalBinds();
		
		// Persist the context
		this->context.Reset(this->isolate, Context::New(this->isolate, NULL, global));
		
		printf("[AliceScript]-[Engine] Done resetting engine\n");
	}

	void ASEngine::Engine::RegisterGlobalBinds()
	{
		/* 'native' Engine functions */
		this->global->Set(String::NewFromUtf8(this->isolate, "print"),
				FunctionTemplate::New(this->isolate, ASEngine::Engine::ASPrintLn));
		this->global->Set(String::NewFromUtf8(this->isolate, "include"),
				FunctionTemplate::New(this->isolate, Server::ASInclude));

		/* Server functions */
		this->global->Set(String::NewFromUtf8(this->isolate, "sendMessageToAll"),
				FunctionTemplate::New(this->isolate, Server::ASSendMessageToAll));
		this->global->Set(String::NewFromUtf8(this->isolate, "getPlayerByID"),
				FunctionTemplate::New(this->isolate, Server::ASGetPlayerById));
		this->global->Set(String::NewFromUtf8(this->isolate, "getMaxClients"),
				FunctionTemplate::New(this->isolate, Server::ASGetMaxClients));

		/* Wrapper wraps & functions */
		Handle<ObjectTemplate> wrapPlayer = ObjectTemplate::New();
		wrapPlayer->SetInternalFieldCount(1);
	}

	bool ASEngine::Engine::Assert(unsigned int argAssert, const v8::FunctionCallbackInfo<v8::Value>& args)
	{
		if(args.Length() != argAssert)
		{
			HandleScope handleScope(this->isolate);
			
			/* Format the error that occurs when function does not have the correct amount of params */
			std::string formatError = std::string();
			if(argAssert == 1)
				formatError = utils::string_format("Function takes %i param but %i given", argAssert, args.Length());
			else if(argAssert > 1)
				formatError = utils::string_format("Function takes %i params but %i given", argAssert, args.Length());
			else
				formatError = utils::string_format("Function takes no params but %i given", argAssert, args.Length());
				
			// Throw the error, event executor will handle the exception
			this->isolate->ThrowException(String::NewFromUtf8(this->isolate, formatError.c_str()));

			return false;
		}

		return true;
	}
	
	/**
	 * Executes a non returning function in JS, also called an event
     * @param eventName
     * @param args
     * @return 
     */
	bool ASEngine::Engine::ExecEvent(std::string eventName, Handle<Value> args[], unsigned int argc)
	{
		HandleScope isolateScope(this->isolate);
		Context::Scope contextScope(this->GetContext());
			
		Handle<String> procName = String::NewFromUtf8(this->isolate, eventName.c_str());
		Handle<Value> procVal   = Server::asEngine->GetContext()->Global()->Get(procName);
		
		/* If the event does not exist in the code */
		if(!procVal->IsFunction())
		{
			printf(CONYEL "[AliceScript]-[Runtime]: %s event not found" CONNRM "\n", eventName.c_str());
			return false;
		}
		
		Handle<Function> func = Handle<Function>::Cast(procVal);
		
		TryCatch tryCatch;
		v8::Local<v8::Function> process =
			v8::Local<v8::Function>::New(Server::asEngine->isolate, func);
		process->Call(Server::asEngine->GetContext()->Global(), argc, args);
		
		Handle<Value> exceptionMsg = tryCatch.Exception();
		
		/* Check to see if the function existed or not and throw an error */
		if(!exceptionMsg.IsEmpty())
		{
			Handle<Message> exceptionInfo = tryCatch.Message();
			String::Utf8Value errorMsg(tryCatch.Exception());
			String::Utf8Value errorLine(exceptionInfo->GetSourceLine());
			String::Utf8Value scriptResourceName(exceptionInfo->GetScriptResourceName());

			/* Trim out trailing tabs from the error line */
			std::string errorLineTrim = std::string(*errorLine);
			errorLineTrim.erase(std::remove(
					errorLineTrim.begin(), errorLineTrim.end(), '\t'), errorLineTrim.end());

			// Construct a string composed of spaces, negate 2 to
			// compensate for the offset of trimming the line
			std::string errorPos = std::string(
					exceptionInfo->GetStartColumn() - 1, ' ') + "^";

			/* Set the position of the error to be under the pointer */
			std::string errorMessage = std::string(*errorMsg);
			int paddingAmount = exceptionInfo->GetStartColumn() - errorMsg.length() / 2;
			if(paddingAmount < 0)
				paddingAmount = 0;
			errorMessage = std::string(paddingAmount, ' ') + errorMessage;
			
			printf(CONRED "[AliceScript]-[Runtime]: Error on line %i res(%s)\n"
					"\t%s\n"
					"\t%s\n"
					"\t%s" CONNRM "\n",
					exceptionInfo->GetLineNumber(),
					*scriptResourceName,
					errorLineTrim.c_str(),
					errorPos.c_str(),
					errorMessage.c_str());

			return false;
		}
		
		return true;
	}
	
	bool ASEngine::Engine::ThrowCompileError(TryCatch& tryCatch, std::string& scriptName)
	{
		Handle<Message> exceptionInfo = tryCatch.Message();
		Handle<Value>   exceptionMsg  = tryCatch.Exception();
		String::Utf8Value errorMsg(tryCatch.Exception());
		String::Utf8Value errorLine(exceptionInfo->GetSourceLine());

		/* Trim out trailing tabs from the error line */
		std::string errorLineTrim = std::string(*errorLine);
		errorLineTrim.erase(std::remove(
				errorLineTrim.begin(), errorLineTrim.end(), '\t'), errorLineTrim.end());

		/* Construct a string composed of spaces, negate 1 to
		   compensate for the offset of trimming the line */
		unsigned int padCnt = 0;
		if(exceptionInfo->GetStartColumn() - 1 > 0)
			padCnt = exceptionInfo->GetStartColumn() - 1;
		std::string errorPos = std::string(padCnt, ' ') + "^";

		/* Set the position of the error to be under the pointer */
		std::string errorMessage = std::string(*errorMsg);
		int paddingAmount = exceptionInfo->GetStartColumn() - errorMsg.length() / 2;
		if(paddingAmount < 0)
			paddingAmount = 0;
		errorMessage = std::string(paddingAmount, ' ') + errorMessage;

		// Print out the error
		printf(CONRED "[AliceScript]-[Compile]: Error while compiling on line %i in '%s'\n"
				"\t%s\n"
				"\t%s\n"
				"\t%s" CONNRM "\n",
				exceptionInfo->GetLineNumber(),
				scriptName.c_str(),
				errorLineTrim.c_str(),
				errorPos.c_str(),
				errorMessage.c_str());

		return false;
	}
	
	bool ASEngine::Engine::ThrowRuntimeError(TryCatch& tryCatch, Handle<Script> script)
	{
		Handle<Message> exceptionInfo = tryCatch.Message();
		Handle<Value>   exceptionMsg  = tryCatch.Exception();
		String::Utf8Value errorMsg(tryCatch.Exception());
		String::Utf8Value errorLine(exceptionInfo->GetSourceLine());

		/* Trim out trailing tabs from the error line */
		String::Utf8Value scriptName(script->GetScriptName());
		std::string errorLineTrim = std::string(*errorLine);
		errorLineTrim.erase(std::remove(
				errorLineTrim.begin(), errorLineTrim.end(), '\t'), errorLineTrim.end());

		/* Construct a string composed of spaces, negate 1 to
		   compensate for the offset of trimming the line */
		unsigned int padCnt = 0;
		if(exceptionInfo->GetStartColumn() - 1 > 0)
			padCnt = exceptionInfo->GetStartColumn() - 1;
		std::string errorPos = std::string(padCnt, ' ') + "^";

		/* Set the position of the error to be under the pointer */
		std::string errorMessage = std::string(*errorMsg);
		int paddingAmount = exceptionInfo->GetStartColumn() - errorMsg.length() / 2;
		if(paddingAmount < 0)
			paddingAmount = 0;
		errorMessage = std::string(paddingAmount, ' ') + errorMessage;

		// Print out the error
		printf(CONRED "[AliceScript]-[Runtime]: Runtime error on line %i in '%s'\n"
				"\t%s\n"
				"\t%s\n"
				"\t%s" CONNRM "\n",
				exceptionInfo->GetLineNumber(),
				*scriptName,
				errorLineTrim.c_str(),
				errorPos.c_str(),
				errorMessage.c_str());

		return false;
	}



// ** 3. Data Returns ****************************************************//
// =======================================================================//

	/**
	 * Attempts to compile a script file and persists it into a script slot,
	 * will return false when there was a compile error.
	 * @param fileName
	 * @return 
	 */
	bool ASEngine::Engine::CompileFileAsScript(std::string fileName)
	{
		HandleScope    isolateScope(this->isolate);
		Context::Scope contextScope(this->GetContext());

		/* Set up a catch scenario, so it catches any compile time
		   errors */
		TryCatch tryCatch;

		Handle<String> file         = this->ReadFile(fileName.c_str());
		Local<Script> compiledFile  = Script::New(file);

		/* Bail out if there's a compile error */
		if(compiledFile.IsEmpty())
		{
			this->ThrowCompileError(tryCatch, fileName);
			return false;
		}

		/* Persist the script into the slot */
		this->scriptSlot.Reset(this->isolate, compiledFile);
		
		printf(CONGRN "[AliceScript]-[Compile]: Compilation successful" CONNRM "\n");

		return true;
	}


// ** 3. Data Returns ****************************************************//
// =======================================================================//

	/**
	 * Gets the Engine's assigned context
	 * @return 
	 */
	Local<Context> ASEngine::Engine::GetContext()
	{
		return Local<Context>::New(this->isolate, this->context);
	}
	
	Local<Script> ASEngine::Engine::GetScript()
	{
		return Local<Script>::New(this->isolate, this->scriptSlot);
	}

	/**
	 * Reads a file into a string for V8 to execute.
	 * 
	 * Copied from https://v8.googlecode.com/svn/trunk/samples/shell.cc
	 * @param isolate
	 * @param name
	 * @return 
	 */
	Handle<String> ASEngine::Engine::ReadFile(const char* name)
	{
		FILE* file = fopen(name, "rb");
		if(file == NULL)
			return Handle<String>();

		fseek(file, 0, SEEK_END);
		int size = ftell(file);
		rewind(file);

		char* chars = new char[size + 1];
		chars[size] = '\0';
		for (int i = 0; i < size;)
		{
			int read = static_cast<int>(fread(&chars[i], 1, size - i, file));
			i += read;
		}
		fclose(file);

		Handle<String> result = String::NewFromUtf8(this->isolate, chars, String::kNormalString, size);
		delete[] chars;

		return result;
	}
	
	

// ** 4. Engine Script Functions *****************************************//
// =======================================================================//

	/**
	 * Prints a line to console
	 * @param args
	 */
	void ASEngine::Engine::ASPrintLn(const v8::FunctionCallbackInfo<Value>& args)
	{
		HandleScope scope(args.GetIsolate());

		Handle<Value> arg = args[0];
		String::Utf8Value str(arg);

		printf("%s\n", *str);
	}
	
	
/**
 * Initialises the globally callable functions
 */
/*void ASEngine::Engine::RegisterBinds()
{
	// Sys Functions
	RootTable(virtualMachine).Func("version", &Server::ASGetAliceVersion);
	RootTable(virtualMachine).Func("rand", &Server::ASRand);
	RootTable(virtualMachine).Func("sleep", &Server::ASSleep);
//	RootTable(virtualMachine).Func("strtok", &SQstrtok);
//	RootTable(virtualMachine).Func("strxpld", &SQstrxpld);
	
	// Global Server Interactive Functions
	RootTable(virtualMachine).Func("GetMaxPlayers", &Server::ASGetMaxClients);
	RootTable(virtualMachine).Func("SendMessageToAll", &Server::ASSendMessageToAll);
	
	RootTable(virtualMachine).Func("GetPlayer", &Server::ASGetPlayer);
	RootTable(virtualMachine).Func("FindPlayersByName", &Server::ASFindPlayersByName);
	RootTable(virtualMachine).Func("FindPlayersByIp", &Server::ASFindPlayersByIp);
	
	RootTable(virtualMachine).Func("LimboAccept", &Server::ASLimboAccept);
	RootTable(virtualMachine).Func("LimboDeny", &Server::ASLimboDeny);
	
	RootTable(virtualMachine).Func("Galloc", &Server::ASDelegateGroups);
	
	RootTable(virtualMachine).Func("GetBanByIp", &Server::ASGetBanByIp);
	
	RootTable(virtualMachine).Func("LoadMap", &Server::ASLoadMap);
	RootTable(virtualMachine).Func("LoadNextMap", &Server::ASLoadNextMap);
	RootTable(virtualMachine).Func("RestartMap", &Server::ASRestartMap);
	RootTable(virtualMachine).Func("ResetMap", &Server::ASResetMap);

	
	// Player Class
	Server::asCPlayer = new Class<Player>(this->virtualMachine);
		Server::asCPlayer->Func("GetId", &Player::SQGetID);
		Server::asCPlayer->Func("GetConnState", &Player::SQGetConnState);
		Server::asCPlayer->Func("GetName", &Player::SQGetName);
		Server::asCPlayer->Func("SetName", &Player::ASSetName);
		Server::asCPlayer->Func("GetGuid", &Player::SQGetGuid);
		Server::asCPlayer->Func("GetIp", &Player::SQGetIp);
		Server::asCPlayer->Func("GetUsername", &Player::ASGetUsername);
		Server::asCPlayer->Func("IsLoggedIn", &Player::ASIsLoggedIn);
		Server::asCPlayer->Func("GetGroup", &Player::ASGetGroup);
		Server::asCPlayer->Func("SendMessage", &Player::SQSendMessage);
		Server::asCPlayer->Func("Kick", &Player::SQKick);
		Server::asCPlayer->Func("Ban", &Player::ASBan);
	RootTable(virtualMachine).Bind("Player", *Server::asCPlayer);
	
	// Player Group Class
	Server::asCGroup = new Class<PlayerGroup>(this->virtualMachine);
		Server::asCGroup->Func("GetName", &PlayerGroup::ASGetName);
		Server::asCGroup->Func("GetRank", &PlayerGroup::ASGetRank);
		Server::asCGroup->Func("HasPermKey", &PlayerGroup::ASHasPermKey);
	RootTable(virtualMachine).Bind("Group", *Server::asCGroup);
	
	// Ban Class
	Server::asCBan = new Class<Ban>(this->virtualMachine);
		Server::asCBan->Func("GetId", &Ban::ASGetId);
		Server::asCBan->Func("GetPlayerIp", &Ban::ASGetPlayerIp);
		Server::asCBan->Func("GetPlayerName", &Ban::ASGetPlayerName);
		Server::asCBan->Func("GetPlayerGuid", &Ban::ASGetPlayerGuid);
		Server::asCBan->Func("GetAdminName", &Ban::ASGetAdminName);
		Server::asCBan->Func("GetReason", &Ban::ASGetReason);
	RootTable(virtualMachine).Bind("Ban", *Server::asCBan);
}*/

ASEngine::Engine::~Engine() {}