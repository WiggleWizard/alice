/* 
 * File:   wrapPlayer.h
 * Author: zinglish
 *
 * Created on 10 March 2014, 22:56
 */

#ifndef WRAPPLAYER_H
#define	WRAPPLAYER_H

#include <v8.h>

class Player;

class WrapPlayer {
public:
	WrapPlayer();
	WrapPlayer(const WrapPlayer& orig);
	virtual ~WrapPlayer();
	
	// ** Binds ****************************************************************
	static void GetName(const v8::FunctionCallbackInfo<v8::Value>& info);
	static void SendMessage(const v8::FunctionCallbackInfo<v8::Value>& info);
	static void GetIp(const v8::FunctionCallbackInfo<v8::Value>& info);
	
	// ** V8 wrapping functions ************************************************
	static v8::Handle<v8::Object> Wrap(v8::Isolate* isolate, Player* player);
	static Player* Unwrap(v8::Handle<v8::Object> obj);
	static v8::Handle<v8::ObjectTemplate> MakeTemplate(v8::Isolate* isolate);
	
private:
	static v8::Persistent<v8::ObjectTemplate> objectTemplate;
};

#endif	/* WRAPPLAYER_H */

