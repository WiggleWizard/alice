#include "wrapPlayer.h"
#include "../player.h"
#include "engine.h"
#include "../server.h"

using namespace v8;

v8::Persistent<v8::ObjectTemplate> WrapPlayer::objectTemplate;

WrapPlayer::WrapPlayer() {}
WrapPlayer::~WrapPlayer() {}

// ** 1. Binds ***********************************************************//
// =======================================================================//

	/**
	 * Gets a player's name
	 * @param info
	 */
	void WrapPlayer::GetName(const v8::FunctionCallbackInfo<Value>& info)
	{
		Server::asEngine->Assert(0, info); // Enforce params
		
		Player* player = Unwrap(info.Holder());
		const std::string name = player->GetName();

		info.GetReturnValue().Set(String::NewFromUtf8(
			info.GetIsolate(), name.c_str(), String::kNormalString, name.length()
		));
	}
	
	/**
	 * Sends a private message to the player
     * @param info
     */
	void WrapPlayer::SendMessage(const v8::FunctionCallbackInfo<Value>& info)
	{
		Server::asEngine->Assert(1, info); // Enforce params
		
		Player* player = Unwrap(info.Holder());
		String::Utf8Value message(info[0]);
		
		player->SendMessage(*message);
	}
	
	void WrapPlayer::GetIp(const v8::FunctionCallbackInfo<Value>& info)
	{
		Server::asEngine->Assert(0, info); // Enforce params
		
		Player* player = Unwrap(info.Holder());
		const std::string ip = player->GetIPAdr();
		
		info.GetReturnValue().Set(String::NewFromUtf8(
			info.GetIsolate(), ip.c_str(), String::kNormalString, ip.length()
		));
	}

// ** 2. V8 wrapping functions *******************************************//
// =======================================================================//

	/**
	 * Wraps a player into a V8 object
	 * @param isolate
	 * @param player
	 * @return 
	 */
	Handle<Object> WrapPlayer::Wrap(Isolate* isolate, Player* player)
	{
		EscapableHandleScope scopeHandle(isolate);

		// Persist the template so we don't have to recreate it
		if(objectTemplate.IsEmpty())
		{
			Handle<ObjectTemplate> rawTemplate = MakeTemplate(isolate);
			objectTemplate.Reset(isolate, rawTemplate);
		}

		// Get a new handle for the template
		Handle<ObjectTemplate> templ = Local<ObjectTemplate>::New(isolate, objectTemplate);

		// Create an empty wrapper
		Local<Object> result = templ->NewInstance();

		// Set the internal field of the wrapped object to the player pointer
		Handle<External> playerPtr = External::New(isolate, player);
		result->SetInternalField(0, playerPtr);

		return scopeHandle.Escape(result);
	}

	/**
	 * Retrieves the player object pointer at internal field 0
	 * @param obj
	 * @return Player pointer
	 */
	Player* WrapPlayer::Unwrap(Handle<Object> obj)
	{
		Handle<External> field = Handle<External>::Cast(obj->GetInternalField(0));
		void* ptr = field->Value();
		return static_cast<Player*>(ptr);
	}

	/**
	 * Creates the template that holds V8 functions
	 * @param isolate
	 * @return Handle to the object's template
	 */
	Handle<ObjectTemplate> WrapPlayer::MakeTemplate(Isolate* isolate)
	{
		EscapableHandleScope scopeHandle(isolate);

		Local<ObjectTemplate> result = ObjectTemplate::New(isolate);
		result->SetInternalFieldCount(1);

		result->Set(
				String::NewFromUtf8(isolate, "getName", String::kInternalizedString),
				FunctionTemplate::New(isolate, WrapPlayer::GetName));
		result->Set(
				String::NewFromUtf8(isolate, "sendMessage", String::kInternalizedString),
				FunctionTemplate::New(isolate, WrapPlayer::SendMessage));
		result->Set(
				String::NewFromUtf8(isolate, "getIP", String::kInternalizedString),
				FunctionTemplate::New(isolate, WrapPlayer::GetIp));

		return scopeHandle.Escape(result);
	}