#ifndef ASENGINE_H
#define	ASENGINE_H

#include "event.h"
#include <string>
#include <vector>
#include <v8.h>

namespace ASEngine {
	
class Engine
{
	public:
		
		Engine();
		virtual ~Engine();
		v8::Isolate* isolate;
		v8::Persistent<v8::Context> context;
		v8::Handle<v8::ObjectTemplate> global;
		
		
		// ** Engine Helpers ***************************************************
		bool Reset();
		void RegisterGlobalBinds();
		bool Assert(unsigned int argAssert, const v8::FunctionCallbackInfo<v8::Value>& args);
		bool ExecEvent(std::string eventName, v8::Handle<v8::Value> args[] = NULL, unsigned int argc = 0);
		bool ThrowCompileError(v8::TryCatch& tryCatch, std::string& script);
		bool ThrowRuntimeError(v8::TryCatch& tryCatch, v8::Handle<v8::Script> script);
		
		// ** Engine Manipulation **********************************************
		bool CompileString(std::string str);
		bool CompileFileAsScript(std::string fileName);
		bool EvaluateScript(v8::Handle<v8::Script> script);
		
		// ** Data Returns *****************************************************
		v8::Local<v8::Context> GetContext();
		v8::Local<v8::Script>  GetScript();
		v8::Handle<v8::String> ReadFile(const char* name);
		
		// ** Engine Script Functions ******************************************
		static void ASPrintLn(const v8::FunctionCallbackInfo<v8::Value>& args);
		static void ASInclude(const v8::FunctionCallbackInfo<v8::Value>& info);

		// SQ Tooling Functions
//		Sqrat::Array SQstrtok(char* s, char* delimeter);
//		Sqrat::Array SQstrxpld(char* s, char* delimeter, unsigned int limit);
//		float ASrand(unsigned int lo, unsigned int hi);

	/** EVENT HELPERS */

	/** =============== */

	private:
		v8::Persistent<v8::Script> scriptSlot;
//		std::vector<v8::Persistent<v8::Script>> scriptSlots;
		void _LogWarning();
};
}

#endif	/* ASENGINE_H */

