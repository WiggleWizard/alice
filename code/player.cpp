#include "player.h"

#include <string>

#include "server.h"
#include "objects/ban.h"
#include "objects/time.h"
#include "database/database.h"
#include "utils.h"

using std::string;

// Initialize the player
Player::Player(unsigned int playerId)
{
    this->id     = playerId;
	this->offset = Player::smClientsLoc + Player::smClientSize * this->id;
	
	// Set Sigil variables
	this->loggedIn = false;
	this->sigilId = 0;
	this->sigilUsername = "";
	this->group = Server::GetGroupById(1); // Set the player's group to Guest by default
}

void Player::Purge()
{
	// Set Sigil variables
	this->loggedIn = false;
	this->sigilId = 0;
	this->sigilUsername = "";
	this->group = Server::GetGroupById(1); // Set the player's group to Guest by default
}

bool Player::IsInGame() {
    return (*GetConnState() == 4);
}

char* Player::GetName() {
    return (char*) _GetPointerToData(mClientOffsets.name);
}

char* Player::GetGuid() {
    return (char*) _GetPointerToData(mClientOffsets.guid);
}

char* Player::GetIPAdr() {
    unsigned long* net1 = (unsigned long*) _GetPointerToData(mClientOffsets.net1);
    void* net2 = _GetDataAsPointer(mClientOffsets.net2);
    unsigned long* net3 = (unsigned long*) _GetPointerToData(mClientOffsets.net3);
    unsigned long* net4 = (unsigned long*) _GetPointerToData(mClientOffsets.net4);
    void* net5 = _GetDataAsPointer(mClientOffsets.net5);
	
    return utils::strxpld((char*) ((AdrToStringFuncType) smAdrToStringFunc)(*net1, net2, *net3, *net4, net5), ":", 1).at(0);
}

char* Player::GetChanBuffer() {
	return (char*)((Player::smClientsLoc + Player::smClientSize * this->id) + Player::mClientOffsets.chanBuffer);
}

/**
 * Gets the player's connection state
 * @return Pointer to connection state
 */
int* Player::GetConnState() {
    return (int*) this->_GetPointerToData(mClientOffsets.state);
}

/**
 * Kicks a player using a client command, if the player has a hack to block the 'w' command then it won't kick them
 * this needs to be patched!
 */
void Player::Kick(char* reason) {
    if (GetConnState() > 0)
        ((mClientCmdFuncType) smSendClientCmdFunc)((void*) (smClientsLoc + smClientSize * this->id), NULL, "%s \"%s\"", "w", reason);
}

/**
 * Bans a player, does not manny kick them...just records the ban
 * @param admin
 * @param reason
 */
/*void Player::IssueBan(Player* admin, string reason)
{
	// The admin has to be logged into Sigil before being able to ban
	if(admin->sigilId > 0)
	{
		// Create a ban object from the provided context
		Ban ban = Ban();
		ban.playerName = this->GetName();
		ban.playerGuid = this->GetGuid();
		ban.playerIp   = this->GetIPAdr();
		ban.adminId    = admin->sigilId;
		ban.adminName  = admin->GetName();
		ban.reason     = reason;
		ban.time       = new Time();
		
		Server::database->IssueBan(ban);
	}
	else
		printf("Cannot ban, banner needs to be logged into a Sigil account\n");
}

void Player::IssueTempBan(Player* admin, string reason, string timeMacro)
{
	u_int32_t weeks = 0;
	u_int32_t days  = 0;
	u_int32_t hours = 0;
	u_int32_t mins  = 0;
}*/

void Player::SetName(char* name)
{
	strncpy((char*)(this->offset + 1616), "name\\", 5);
	strncpy((char*)(this->offset + 1616 + 5), name, 15);

	((mPlayerForceUserinfo)smPlayerForceUserinfoLoc)(this->offset);
	((mPlayerForceName)smPlayerForceNameLoc)(this->id);
}

void Player::SendMessage(char* message)
{
	if (GetConnState() > 0)
        ((mClientCmdFuncType) smSendClientCmdFunc)((void*) (smClientsLoc + smClientSize * this->id), NULL, "%s \"%s\"", "h", message);
}

/**
 * 
 * S Q  F U N C T I O N S
 * 
 */


/*unsigned int Player::SQGetID()
{
	return this->id;
}
int Player::SQGetConnState()
{
	return *((int*) this->_GetPointerToData(mClientOffsets.state));
}
Sqrat::string Player::SQGetName() {
    return (char*) _GetPointerToData(mClientOffsets.name);
}
void Player::ASSetName(char* name) {
    this->SetName(name);
}
Sqrat::string Player::SQGetGuid() {
    return (char*) _GetPointerToData(mClientOffsets.guid);
}
Sqrat::string Player::SQGetIp() 
{
    unsigned long* net1 = (unsigned long*) _GetPointerToData(mClientOffsets.net1);
    void* net2 = _GetDataAsPointer(mClientOffsets.net2);
    unsigned long* net3 = (unsigned long*) _GetPointerToData(mClientOffsets.net3);
    unsigned long* net4 = (unsigned long*) _GetPointerToData(mClientOffsets.net4);
    void* net5 = _GetDataAsPointer(mClientOffsets.net5);

    return (char*) ((AdrToStringFuncType) smAdrToStringFunc)(*net1, net2, *net3, *net4, net5);
}
Sqrat::string Player::ASGetUsername()
{
	return this->sigilUsername;
}


void Player::SQKick(char* reason) {
    if (GetConnState() > 0)
        ((mClientCmdFuncType) smSendClientCmdFunc)((void*) (smClientsLoc + smClientSize * this->id), NULL, "%s \"%s\"", "w", reason);
}
void Player::ASBan(Player* admin, string reason)
{
	this->IssueBan(admin, reason);
}
PlayerGroup* Player::ASGetGroup()
{
	return this->group;
}
bool Player::ASIsLoggedIn()
{
	return this->loggedIn;
}*/


/**
 * 
 * P R I V A T E  F U N C T I O N S
 * 
 */
void* Player::_GetDataAsPointer(const unsigned long offset) {
    return (void*) *((unsigned long*) ((Player::smClientsLoc + Player::smClientSize * this->id) + offset));
}

void* Player::_GetPointerToData(const unsigned long offset) {
    return (void*) ((Player::smClientsLoc + Player::smClientSize * this->id) + offset);
}

Player::~Player()
{
    
}