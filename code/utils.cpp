/* 
 * File:   utils.cpp
 * Author: zinglish
 * 
 * Created on 11 November 2013, 03:40
 */

#include "utils.h"

#include <stdarg.h>
#include <cstddef>
#include <string>
#include <cstring>
#include <memory>

/**
 * Explodes a string without limit
 * @param s
 * @param delim
 * @return 
 */
std::vector<char*> utils::strxpld(char* s, char* delim)
{
	std::vector<char*> arr;
	char* pch = strtok(s ,delim);
	
	while(pch != NULL)
	{
		arr.push_back(pch);
		pch = strtok(NULL, delim);
	}
	
	return arr;
}

/**
 * Explodes a string but limits the tokens
 * @param s Input string
 * @param delim
 * @param limit
 * @return
 */
std::vector<char*> utils::strxpld(char* s, char* delim, unsigned int limit)
{
	int delimlen = strlen(delim);
	char* sp;
	char* cp = s;
	std::vector<char*> arr;
	
	for(unsigned int i=0;i<limit;i++)
	{
		if((sp = strstr(cp, delim)) == NULL)
			break;
		
		for(int j=0;j<delimlen;j++)
			*(sp+j) = 0x00;
		
		arr.push_back(cp);
		
		cp = sp + delimlen;
	}
	if(*(cp) != '\0')
		arr.push_back(cp);
	
	return arr;
}

std::string utils::string_format(const std::string fmt_str, ...) {
    int final_n, n = fmt_str.size() * 2; /* reserve 2 times as much as the length of the fmt_str */
    std::string str;
    std::unique_ptr<char[]> formatted;
    va_list ap;
    while(1) {
        formatted.reset(new char[n]); /* wrap the plain char array into the unique_ptr */
        strcpy(&formatted[0], fmt_str.c_str());
        va_start(ap, fmt_str);
        final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
        va_end(ap);
        if (final_n < 0 || final_n >= n)
            n += abs(final_n - n + 1);
        else
            break;
    }
    return std::string(formatted.get());
}