#include "../server.h"
#include "../utils.h"

#include <vector>

#include "../hook.h"
#include "../player.h"
#include "../objects/ban.h"

#include "../database/database.h"

#include "../asengine/engine.h"

// Initialize variables
unsigned int* Server::maxClients;
vector<Player*> Server::players;
ASEngine::Engine* Server::asEngine;

vector<Server::Limbo> Server::limboConnections;
char Server::aliceVersion[21];
unsigned int Server::communityId;
unsigned int Server::serverId;
Database* Server::database;
vector<PlayerGroup*> Server::groups;
char* Server::cfgChunk;
vector<char*> Server::cfgChunkLines;


/**
 * Initializes the server hooks and any server variables
 */
void Server::Init(unsigned int serverId)
{
	// Generate the version number
	sprintf(aliceVersion, "%d.%d.%d", versionMajor, versionMinor, versionPatch);
	
	// Assign community ref and server ID
	Server::communityId = 1;
	Server::serverId = serverId;
	
	// Connect to the database and do some db initialization functions
	Server::database = new Database();
	
	// We need to connect to the database and fetch a bunch of data about this server based on what's in Metro
	Server::database->PrintDebug("Attempting to connect to Metro");
//	if(Server::database->Connect("tcp://192.81.221.234:3306", "metrouser", "23vre9nu2re923n238rhf28", "metro"))
	if(Server::database->Connect("tcp://localhost:3306", "root", "terence", "metro"))
		Server::database->PrintDebug("Connection seems ok, moving on");
	else
		Server::database->PrintFatal("Connection attempt failed, Alice will now exit");
	
	// Sets the server's ID and community ID along with anything else the server needs to know before starting up properly
	if(!Server::database->SetupServer())
		Server::database->PrintFatal("Server was not set up correctly, now exiting");
	
	// Register the hooks
	RegisterHooks();
}

/**
 * Creates hooks into the server's memory
 */
void Server::RegisterHooks()
{	
	// When the common server stuff has been initialized
	hServerCommonInit = new Hook((void*) smServerCommonInitLoc, 5, (void*) HServerCommonInit);
	// When the server is initialized
	hServerInit = new Hook((void*) smServerInitLoc, 5, (void*) HServerInit);
	// When a player requests to join
	hPlayerJoinRequest = new Hook((void*) smPlayerJoinRequestLoc, 5, (void*) HPlayerJoinRequest);
	// When a player disconnects from the server
	hPlayerDisconnect = new Hook((void*) smPlayerDisconnectLoc, 5, (void*) HPlayerDisconnect);
	// When a player join request is denied
//	hPacketResponse = new Hook((void*) smPacketResponseLoc, 5, (void*) HPacketResponse);
	// When the player says something
	hPlayerSay = new Hook((void*) smPlayerSayLoc, 5, (void*) HPlayerSay);
	// When an RCON "status" is requested
	hServerStatusRequest = new Hook((void*) 0x0816C708, 5, (void*) HServerStatusRequest);
	// When the player changes one of his server affecting variables client side
	hPlayerNameChange = new Hook((void*) smPlayerNameChangeLoc, 5, (void*) HPlayerNameChange);
	
	// Patches
//	hEnsureAppendage = new Hook((void*) smEnsureAppendage, 5, (void*) HEnsureAppendage);
//	hReadCfg = new Hook((void*) smReadCfgLoc, 5, (void*) HReadCfg);
//	hCvarCreateInt = new Hook((void*) smCvarCreateInt, 5, (void*) HCvarCreateInt);
}

/**
 * 
 * C O M M O N  F U N C T I O N S
 * 
 */

/**
 * Gets the number of clients allowed to connect this session
 * @return Pointer to the max clients cvar on the server
 */
unsigned int Server::GetMaxClients()
{ 
	return *(unsigned int*) ((void*) smMaxClientsLoc);
}

/**
 * Gets a PlayerGroup pointer by its Sigil ID
 * @param id
 * @return 
 */
PlayerGroup* Server::GetGroupById(unsigned int id)
{
	int groupsSize = Server::groups.size();
	for(unsigned int i=0;i<groupsSize;i++)
	{
		if(groups.at(i)->id == id)
			return groups.at(i);
	}
	
	return NULL;
}

/**
 * Executes a single command, exactly how it's typed into console
 * @param cmd The full command
 */
int Server::ExecCmd(std::string cmd)
{
//	Server::PrintDebug(utils::string_format("Exec Command: %s", cmd));
	return ((Server::mExecCmd)Server::smExecCmdLoc)(0, 0, cmd.c_str());
}

/**
 * Executes an entire config (From string)
 * @param cfg
 */
void Server::ExecConfig(std::string cfg)
{
	// Ensure we destroy what ever was in the server's local config first
	delete Server::cfgChunk;
	for(unsigned int i=0;i<Server::cfgChunkLines.size();i++)
		delete Server::cfgChunkLines.at(i);
	Server::cfgChunkLines.clear();
	
	// We need to break the string out by newline
	Server::cfgChunk = new char[cfg.length()];
	strcpy(Server::cfgChunk, cfg.c_str());
	Server::cfgChunkLines = utils::strxpld(Server::cfgChunk, "\n");
	
	for(unsigned int i=0;i<Server::cfgChunkLines.size();i++)
		Server::ExecCmd(Server::cfgChunkLines.at(i));
}

Player* Server::GetPlayerByOffset(u_int32_t offset)
{
	return Server::players.at((offset - 0x090B4F8C) / 677436);
}

u_int32_t Server::GetPlayerIdByOffset(u_int32_t offset)
{
	return (offset - 0x090B4F8C) / 677436;
}

/**
 * 
 * D E B U G  P R I N T I N G
 * 
 */
void Server::LogASNoEvent(const char* funcName)
{
	printf("Event [%s] not found\n", funcName);
}

void Server::PrintDebug(std::string message)
{
	printf("Alice | DEBUG | %s\n", message.c_str());
}
void Server::PrintLimboDebug(std::string message)
{
	printf("Alice | LIMBO DEBUG | %s\n", message.c_str());
}
void Server::PrintFatal(std::string message)
{
	printf("Alice | FATAL | %s\n", message.c_str());
}
void Server::PrintNoEventFound(std::string message)
{
	printf("AliceScript | NO EVENT | %s\n", message.c_str());
}

void* Server::ThreadPlayerLogin(void* _arg)
{
	
}


// Print Header
void Server::PrintBanner()
{
	// Print information
	printf("\n\n\n                LGt                                                               \n");
	printf("                  tGG                                                             \n");
	printf("                   fGGG                                                           \n");
	printf("                   ,GGGf                                                          \n");
	printf("                    GGGG                                                          \n");
	printf("                   :GGGG                ,GGG                                      \n");
	printf("                   GGGGG.               GGGG             ..                       \n");
	printf("                   GGGGG    ft,,f:C:    GGG,           CGGGG                      \n");
	printf("                  lGGGGGf   f,lfGGGGG      .         iGGGG:GG              GG     \n");
	printf("                  GGtGGGG     GGGG    CLLGGG        GGGl  .CG             GGL     \n");
	printf("                 CGGtGGGG     :GGG     CGGG        GGGf.  fG.       lCGGGGGG      \n");
	printf("                 GGG GGGG     .GGG     tGGG       tGGG     .   LGGGGGGGGGGG       \n");
	printf("                ,GGG GGGG     :GGG     GGGG       GGGf.         iGGLGGCti;        \n");
	printf("                GGGG GGGG.    lGGG      GGG      lGGG            GGG:             \n");
	printf("               tGGG. GGGG:    fGGG.     GGG      GGGf            GGG              \n");
	printf("               GGGG  GGGGL    CGGG      GGG     fGGG             GGG;             \n");
	printf("              iGGG   GGGGG    GGGG      GGG     CGGf             GGGG             \n");
	printf("       :;G    GGGGC  .GGGG    GGGG      GGG     GGGf             CGGG   :.        \n");
	printf("         LfltGGGGGGGG GGGG    GGGG      GGG     GGGG             fGGG tGGG        \n");
	printf("         .GGGGGGCffGGGGGGG    GGGG      GGG     GGGt             CGGGtGGGG        \n");
	printf("           tGGGGG:,i;GGGGG    GGGG      GGG.    GGGG             .GGGGGGCCG       \n");
	printf("            CGGGl   ..GGGGG   GGGG      GGGi    GGGG              GGGGL  CG       \n");
	printf("            GGGG     .GGGGG   GGGG      GGGGG   LGGG        .L    GGG     .       \n");
	printf("           GGGG:      GGGGG   GGGG    ..GCCGC    GGGG       .CG   GGG:            \n");
	printf("          .GGGG       CGGGG   GGGG     G         iGGG        GG   GGG;         G  \n");
	printf("          CGGGC       ,GGGG   GGGGCGG .           lGGG      iGG   GGGG         GG \n");
	printf("  t      CGGGG        ,GGGG   GGGGGGGGGf .    G.   fGGC   tGGGt   GGGGGlGG:;  :GG \n");
	printf("  GGG    GGGGG         GGGG   GGiLCCGGGGGG    ,G    ,GGGGGGGG,    GGGGGGGGGGGGGG .\n");
	printf("  Gt    ,GGGG          GGGGL  i; .  lilGGGGGLGG.      .CGGC      .GGGGffCCGCGGGG. \n");
	printf("  GG    GGGG:          GGGGG           l;tGGGGf                 GGGf  .  .tlifl   \n");
	printf("  GGi,CGGGC            GGGGG             ,i:L;                  .,                \n");
	printf("   GGGGGl.             GGLGGC  t                                                  \n");
	printf("                     ;GGL:;GGCG:                                                  \n");
	printf("                     tl      i                                                    \n\n\n");
	
	printf("==================================================================================\n");
	printf("#\n");
	printf("#    Alice\n");
	printf("#    Inspired by Alice in Wonderland\n");
	printf("#\n");
	printf("#    Written by.......: Terence-Lee \"Zinglish\" Davis\n");
	printf("#    Email............: zinglish@gmail.com\n");
	printf("#    Dev Wiki.........: wiki.aliceserver.com\n");
	printf("#    Alice version....: %s\n", aliceVersion);
	printf("#    Build No.........: \n");
	printf("#    Build Hash.......: \n");
	printf("#\n");
	printf("#    Reserved\n");
	printf("#\n");
	printf("==================================================================================\n\n");
	
//	thread test = thread(Server::HTServerStatusRequest);
}
