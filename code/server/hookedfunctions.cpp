
/******************************************************************************
 * 
 * Do not think of these functions as all connected in some way shape or form,
 * they are all completely separate and are just kept here as a collection.
 * 
 * 
 * - Table of Contents
 * ==================================
 *		1. Server Common Init
 *			- HServerCommonInit
 *			- HServerInit
 *		2. Player join and disconnect
 *			- HPlayerJoinRequest
 *			- HPlayerDisconnect
 *		3. Player ingame events
 *			- HPlayerSay
 *			- HPlayerNameChange
 *  
 *****************************************************************************/

#include "../server.h"

#include <v8.h>
#include <pthread.h>
#include <time.h>

#include "../asengine/engine.h"
#include "../database/database.h"
#include "../patch.h"
#include "../hook.h"

#include "../utils.h"

using namespace ASEngine;
using namespace v8;

//|============================================
//| 
//| VAR INIT
//|
//|============================================
	Hook* Server::hServerCommonInit;
	Hook* Server::hServerInit;
	Hook* Server::hPlayerJoinRequest;
	Hook* Server::hPlayerDisconnect;
	Hook* Server::hPacketResponse;
	Hook* Server::hPlayerSay;
	Hook* Server::hServerStatusRequest;
	Hook* Server::hPlayerNameChange;

	Hook* Server::hEnsureAppendage;
	Hook* Server::hReadCfg;
	Hook* Server::hCvarCreateInt;
	
	unsigned int Server::cfgMaxClients;
	char*        Server::cfgServerName;
	unsigned int Server::cfgPort;
	
	
	
	/**
	* Called when RCON command "status" is executed
	*/
	void Server::HServerStatusRequest()
	{
		HandleScope isolateScope(Server::asEngine->isolate);
		Context::Scope contextScope(Server::asEngine->GetContext());
		
		// Execute the event
		Server::asEngine->ExecEvent("onStatusRequest");
		
		// Patch the communication buffer to a higher limit her
		//*(char*)0x81785E2 = 0x15; // Patch comms short byte

		/*((mComBuffFuncType) smComBuffLoc)(0, "total_ingame: %i\n", ingameClients);*/
	}

   
// ** 1. Server Common Init **********************************************//
// =======================================================================//
	
	/**
	 * Called just before the server
	 * 
	 * @param a1
	 * @return 
	 */
	int Server::HServerCommonInit(int a1)
	{
		// Reconstruct the original function and execute it
		hServerCommonInit->UnHook();
		int rtn = ((mServerCommonInit)smServerCommonInitLoc)(a1);

		 /** Manual Patches */
		//	Patch test = Patch();
		//	test.Blockout((void*)0x08170AE6, 138);

		// Execute the user config, set default values and ensure map rotation
		Server::PrintDebug("Executing Sigil stored config");
		Server::ExecConfig(Server::database->GetConfig());
		Server::ExecCmd("rcon_password \"\"");
		Server::ExecCmd(utils::string_format("sv_hostname \"%s\"", Server::cfgServerName).c_str());
		Server::ExecCmd(utils::string_format("sv_maxclients %d", Server::cfgMaxClients).c_str());
		Server::ExecCmd("map_rotate");

		return rtn;
	}

	
	/**
	 * Called when the server is initialised correctly
	 * Creates SQ Engine and its binds, also does the bulk of the application loading and constructions
	 * 
	 * @param a1
	 * @return
	 */
	int Server::HServerInit(int a1)
	{
		// Reconstruct the original function and execute it
		hServerInit->UnHook();
		int rtn = ((mServerInitFuncType)smServerInitLoc)(a1);

		// Code after the server has fully initialised
		printf("-----------------------------------\n"); // Just some eye candy really
		PrintBanner();

		// Echo out some debugging crap
		Server::PrintDebug(utils::string_format("Community ID: %i", Server::communityId));
		Server::PrintDebug(utils::string_format("Server ID: %i", Server::serverId));

		// Compile groups
		Server::database->AllocateGroups(&Server::groups);

	   /** Variable assignment and delegation */
		maxClients = (unsigned int*) ((void*) smMaxClientsLoc);
		players.reserve(*maxClients);

		for(unsigned int i=0;i<*maxClients;i++)
		{
			players.push_back(new Player(i));
		}
		
		// Prep the limbo connection vector
		Server::limboConnections.reserve(*maxClients);


	   /** Construction of the AliceScript Engine */
		Server::asEngine = new ASEngine::Engine();

		// Run the script
		HandleScope isolateScope(Server::asEngine->isolate);
		Context::Scope contextScope(Server::asEngine->GetContext());

		Server::asEngine->CompileFileAsScript("/var/cod4server/scripts/main.js");
		Server::asEngine->GetScript()->Run();
		
		// Execute the event
		Server::asEngine->ExecEvent("onServerInit");
			
		return rtn; // Return what the original function has returned
	}
	
	
// ** 2. Player join and disconnect **************************************//
// =======================================================================//
	
	/**
	 * The main function of this method is to check for limbo connections and accept them or deny
	 * them accordingly, then call the AliceScript event function
	 * 
	 * The information that is passed in this function is part of a C++ Net struct
	 * 
	 * @param a1
	 * @param a2
	 * @param a3 Unique identifier for each connection, possibly
	 * @param a4
	 * @param a5
	 * @return 
	 */
	bool Server::HPlayerJoinRequest(unsigned long a1, void* a2, unsigned long uniq, unsigned long a4, void* a5)
	{
		bool hasLimbo = false;
		unsigned int limboSize = limboConnections.size();
		std::string limboMessage;

		// Get the IP address of the player requesting a join and sanitize it
		char* playerAdr = ((mAdrToStringFuncType) smAdrToStringLoc)(a1, a2, uniq, a4, a5);
		utils::strxpld(playerAdr, ":", 1);

		// Loop through the list of limbo connections
		for(unsigned int i=0;i<limboSize;i++)
		{
			Limbo limbo = limboConnections.at(i);

			// If it finds the IP
			if(strcmp(limbo.ip, playerAdr) == 0)
			{
				limboMessage = utils::string_format("Incoming request for IP ", limbo.ip);
				PrintLimboDebug(limboMessage);
				
				hPlayerJoinRequest->UnHook();
					((mPlayerJoinRequest) smPlayerJoinRequestLoc)(a1, a2, uniq, a4, a5);
					hPlayerJoinRequest = new Hook((void*) smPlayerJoinRequestLoc, 5, (void*) HPlayerJoinRequest);

				if(limbo.state == 1)
				{
					limboMessage = utils::string_format(limboMessage, "Limbo %s was accepted\n", limbo.ip);
					PrintLimboDebug(limboMessage);

					// Rebuild the original function and call it
					hPlayerJoinRequest->UnHook();
					((mPlayerJoinRequest) smPlayerJoinRequestLoc)(a1, a2, uniq, a4, a5);
					hPlayerJoinRequest = new Hook((void*) smPlayerJoinRequestLoc, 5, (void*) HPlayerJoinRequest);

					limboConnections.erase(limboConnections.begin() + i);
				}
				else if(limbo.state == 2)
				{
					limboMessage = utils::string_format("Limbo %s was denied, reason: %s\n", limbo.ip, limbo.denyReason.c_str());
					PrintLimboDebug(limboMessage);

					limbo.denyReason.insert(0, "error\n");

					((mPacketResponse) smPacketResponseLoc)(1, a1, a2, uniq, 0x000000, 0x000000, limbo.denyReason.c_str());

					limboConnections.erase(limboConnections.begin() + i);
				}

				hasLimbo = true;
				break;
			}
		}

		// If the IP and uniq has no limbo then create one for it
		if(!hasLimbo)
		{
			limboConnections.push_back(Limbo());
			limboSize = limboConnections.size();
			limboConnections.at(limboSize - 1).ip = playerAdr;

			limboMessage = utils::string_format("IP added to limbo queue: %s", limboConnections.at(limboSize - 1).ip);
			PrintLimboDebug(limboMessage);
			
			// Execute the event
			Server::asEngine->ExecEvent("onIPJoinRequest");
		}

		return false;
	}

	/**
	 * Triggered when the player leaves the server
	 * 
	 * @param playerOffset
	 * @param a2
	 * @param reason
	 * @return 
	 */
	bool Server::HPlayerDisconnect(unsigned long playerOffset, void* a2, unsigned int reason)
	{
		unsigned int playerId = (playerOffset - 0x090B4F8C)/677436;

		// Clean out the player object
		players.at(playerId)->Purge();

		// Execute the AS event

		// Run the function like normal then rehook it
		hPlayerDisconnect->UnHook();
		((mPlayerDisconnect)smPlayerDisconnectLoc)(playerOffset, a2, reason);
		hPlayerDisconnect->Rehook();
	}

	
	
// ** 3. Player ingame events ********************************************//
// =======================================================================//

	/**
	 * Triggered when the player says something
	 * 
	 * This function also bypasses the script if !login command is issued on the server
	 * @param a1 Pointer to the inet object
	 * @param a2
	 * @param a3 Sayteam produces 1 and say produces 0
	 * @param a4 Pointer to the character for message
	 * @return 
	 */
	int Server::HPlayerSay(unsigned int playerId, int a2, int teamSay, int pMessage)
	{
		// Cleanse the message if the weird character that appears before each message
		std::string msg = std::string((char*) pMessage);
		if(((char*) pMessage)[0] == 0x15)
		{
			msg.erase(0, 1);
		}

		// We wanna bypass the script if the player uses !login
		if(msg.find("!login ") == 0)
		{
			std::vector<char*> split = utils::strxpld((char*) pMessage, " ", 2);
			if(split.size() == 3)
			{
				// Allocate memory for the argument structure of the pthread
				struct Server::ThreadPlayerLoginArg* threadArg =
					(Server::ThreadPlayerLoginArg*) malloc(sizeof(*threadArg));
				pthread_t thread;
				pthread_create(&thread, NULL, Server::ThreadPlayerLogin, threadArg);
				
				// If the login was a success
				if(Server::database->LogPlayerIn(split.at(1), split.at(2), players.at(*(int*) playerId)))
				{
				}
				else // If it failed
				{
				}
			}
			else if(split.size() < 3)
			{
			}
			return 1;
		}
		// Cold boot that bitch!!!
		else if(msg.find("!nyan") == 0)
		{
			Server::asEngine->Reset();
			
			HandleScope isolateScope(Server::asEngine->isolate);
			Context::Scope contextScope(Server::asEngine->GetContext());
			
			Server::asEngine->CompileFileAsScript("/var/cod4server/scripts/main.js");
			Local<Script> s = Server::asEngine->GetScript();
			s->Run();
			
			// Call the cold boot function in the script
			Server::asEngine->ExecEvent("onScriptColdBoot", NULL);
		}
		// Calls the 
		else
		{
			HandleScope isolateScope(Server::asEngine->isolate);
			Context::Scope contextScope(Server::asEngine->GetContext());
			
			/* Create the args */
			Handle<Value> args[] = {
				v8::Integer::NewFromUnsigned(Server::asEngine->isolate, *(unsigned int*) playerId),
				String::NewFromUtf8(Server::asEngine->isolate, msg.c_str())
			};
			
			// Call the event
			Server::asEngine->ExecEvent("onPlayerSay", args, 2);
		}

		return 1;
	}
	
	/**
	 * Called when a player attempts to change his name or connection affected
	 * variable.
	 * 
     * @param playerOffset
     * @return 
     */
	int Server::HPlayerNameChange(u_int32_t playerOffset)
	{
		Player*  player  = Server::GetPlayerByOffset(playerOffset);
		char*    newName = ((mGetValueFromSlashString)smGetValueFromSlashString)((char*)(0x0887C320 + 9), "name");
		unsigned int rtn = 0;

		HandleScope isolateScope(Server::asEngine->isolate);
		Context::Scope contextScope(Server::asEngine->GetContext());
		
		/* Create the args */
		Handle<Value> args[] = {
			v8::Integer::NewFromUnsigned(Server::asEngine->isolate, player->id),
			String::NewFromUtf8(Server::asEngine->isolate, newName)
		};
		
		// Call the event
		Server::asEngine->ExecEvent("onPlayerAttemptNameChange", args, 2);

		return rtn;
	}

	
	
	





//bool Server::HPacketResponse(signed int a1, int a2, int a3, int a4, char a5, char a6, const char *a7)
//{
//	hPacketResponse->UnHook();
//	bool rtn = ((mPlayerJoinDeny)smPacketResponseLoc)(a1, a2, a3, a4, a5, a6, a7);
//	// HPlayerJoinDeny(1, 4, 202000A, 2371, null, null, error\nEXE_SERVER_IS_DIFFERENT_VER1.7)
//	
//	// When a player join request is denied
//	hPacketResponse = new Hook((void*) smPacketResponseLoc, 5, (void*) HPacketResponse);
//	
//	printf(">>> HPacketResponse(%X, %X, %X, %X, %X, %d, %s) = %d\n", a1, a2, a3, a4, a5, a6, a7, rtn);
//	
//	return rtn;
//}

const char* Server::HEnsureAppendage(const char *filename, unsigned int a2, const char* append)
{
	return filename;
}

/**
 * Triggered when a config or text file is read for setting console variables
 * @param a1
 * @param a2
 * @param a3
 * @return 
 */
const char* Server::HReadCfg(void* a1, unsigned long* a2, void* a3)
{
	char* filename = (char*)a1;
	
	hReadCfg->UnHook();
	const char* rtn = ((mReadCfg)smReadCfgLoc)(a1, a2, a3);
	hReadCfg->Rehook();
	
	return rtn;
}

/**
 * Called when the server creates a console variable
 * @param varName
 * @param defaultValue
 * @param min
 * @param max
 * @param maxLen
 * @param varDesc
 * @return The array's address
 */
unsigned long Server::HCvarCreateInt(char* varName, unsigned long defaultValue, unsigned long min, unsigned long max, unsigned long maxLen, char* varDesc)
{
	// Set the server's port just before the variable is created to ensure that the server runs on this port
	if(strcmp(varName, "net_port") == 0)
	{
		if(Server::cfgPort > 0)
		{
			Server::PrintDebug("Setting net_port manually");
			Server::ExecCmd(utils::string_format("net_port %d", Server::cfgPort));
		}
		else
		{
			Server::PrintFatal("No port found, now exiting");
			exit(0);
		}
	}
	
	// Run the function like normal
	Server::hCvarCreateInt->UnHook();
	unsigned int rtn = ((mCvarCreateInt)smCvarCreateInt)(varName, defaultValue, min, max, maxLen, varDesc);
	Server::hCvarCreateInt->Rehook();

	// Debugging
//	printf("--------------------------------------------------\n");
//	printf("| Creating Cvar: \"%s\"\n", varName);
//	printf("| - Desc: %s\n", varDesc);
//	printf("| - Default: %i\n", defaultValue);
//	printf("| - Minmax: %i | %i\n", min, max);
//	printf("|\n");
//	printf("| - Stored Address: %X\n", rtn);
//	printf("--------------------------------------------------\n");
	
	return rtn;
}