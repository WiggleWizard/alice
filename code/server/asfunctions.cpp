
/************************************************** 
 * 
 * - Table of Contents
 * ==================================
 *		1. Frequently Used
 *		2. Player Functions
 *		3. Limbo Functions
 *		4. Group Functions
 *		5. Gameplay Changing Functions
 *  
 **************************************************/

#include "../server.h"

#include <time.h>

#include "../asengine/engine.h"
#include "../database/database.h"
#include "../objects/ban.h"
#include "../utils.h"

#include "../asengine/wrapPlayer.h"

#include <v8.h>
using namespace v8;

// ** 1. FREQUENTLY USED *************************************************//
// =======================================================================//

	// Returns Alice's version
	char* Server::ASGetAliceVersion()
	{
		return aliceVersion;
	}
	
	void Server::ASSleep(unsigned long msec)
	{
		struct timeval bt1, bt2;
		struct timespec t1, t2;
		t1.tv_sec  = msec / 1000;
		t1.tv_nsec = (msec % 1000) * 1000000;
		
//		printf("Attempting to sleep for %is (%luns)\n", t1.tv_sec, t1.tv_nsec);

//		gettimeofday(&bt1, NULL);
		nanosleep(&t1, &t2);
//		gettimeofday(&bt2, NULL);
		
//		double td1 = bt1.tv_sec + (bt1.tv_usec/1000000.0);
//		double td2 = bt2.tv_sec + (bt2.tv_usec/1000000.0);
//		double diff = td2 - td1;
//		
//		printf("Sleep length -> %16.11f s\n", diff);
	}

	// Gets the max clients that can connect to this server
	void Server::ASGetMaxClients(const v8::FunctionCallbackInfo<Value>& info)
	{
		if(!Server::asEngine->Assert(0, info)) return;
		
		info.GetReturnValue().Set(
			v8::Integer::NewFromUnsigned(Server::asEngine->isolate, *Server::maxClients)
		);
	}

	/**
	 * Gets a Player object
     * @param playerId
     * @return Player
     */
	void Server::ASGetPlayerById(const v8::FunctionCallbackInfo<Value>& info)
	{
		Handle<Value> arg = info[0];
		String::Utf8Value argChar(arg);
		unsigned int playerId = atoi(*argChar);
		
		if(playerId < *(Server::maxClients) && playerId >= 0)
		{
			info.GetReturnValue().Set(
				WrapPlayer::Wrap(Server::asEngine->isolate, players.at(playerId))
			);
			
			return;
		}
		
		info.GetReturnValue().Set(v8::Null(Server::asEngine->isolate));
	}
	
	/**
	 * Sends a message to all connected players
	 * @param message
	 */
	void Server::ASSendMessageToAll(const v8::FunctionCallbackInfo<v8::Value>& args)
	{
		if(!Server::asEngine->Assert(1, args)) return;

		Player* player;
		Handle<Value> arg = args[0];
		String::Utf8Value str(arg);

		for(unsigned int i = 0; i<*maxClients; i++)
		{
			player = players.at(i);

			if(*player->GetConnState() != 4)
				continue;

			player->SendMessage(*str);
		}
	}
	
	void Server::ASInclude(const v8::FunctionCallbackInfo<Value>& args)
	{
		HandleScope scope(Server::asEngine->isolate);
		
		// Get the filename from the argument
		String::Utf8Value fileName(args[0]);
		std::string scriptName = std::string(*fileName);
		
		// Read the file into value
		Handle<String> file = Server::asEngine->ReadFile(*fileName);
		
		if(!file.IsEmpty())
		{
			/* Attempt a compile, bail out if failed */
			TryCatch tryCatch;
			Handle<Script> compiledFile = Script::New(file);
			printf("Script name: %s\n", compiledFile->GetScriptName());
			
			if(compiledFile.IsEmpty())
			{
				Server::asEngine->ThrowCompileError(tryCatch, scriptName);
				return;
			}
			
			printf("Including file\n");
			compiledFile->Run();
			
			/* Check for runtime errors, bail out if there is one */
			Handle<Value> exceptionMsg = tryCatch.Exception();
			if(!exceptionMsg.IsEmpty())
			{
				printf("Exception occured\n");
//				Persistent<Script> persistentScript;
//				persistentScript.Reset(Server::asEngine->isolate, compiledFile);
				
				Server::asEngine->ThrowRuntimeError(tryCatch, compiledFile);
			}
			
			return;
		}
		
		printf("File is empty");
	}

   

// ** 2. PLAYER SEARCH ***************************************************//
// =======================================================================//
	
	/**
	 * Returns an array of players based on a partial name search
	 * @param partialName
	 * @return 
	 */
	/*Sqrat::Array Server::ASFindPlayersByName(char* partialName)
	{
		// If the partial name is larger than max playername size then don't bother
		if(sizeof(*partialName) > PLAYERNAME_MAX_SIZE){ return NULL; }

		// Otherwise continue with the function
		Sqrat::Array arr(Server::asEngine->GetVM());
		std::string searchName;
		Player* player;

		for(unsigned int i=0;i<*maxClients;i++)
		{
			player = players.at(i);

			if(*player->GetConnState() == 0){ continue; } // If the player is not connected

			searchName = player->GetName();

			if(searchName.find(partialName, 0) != -1)
			{
				arr.Append(players.at(i));
			}
		}
		return arr;
	}*/

	/**
	 * Returns an array of players based on a partial IP Address search
	 * @param partialIp
	 * @return 
	 */
	/*Sqrat::Array Server::ASFindPlayersByIp(char* partialIp)
	{
		// If the partial name is larger than max playername size then don't bother
		if(sizeof(*partialIp) > PLAYERNAME_MAX_SIZE){ return NULL; }

		// Otherwise continue with the function
		Sqrat::Array arr(Server::asEngine->GetVM());
		std::string searchIp;
		Player* player;

		for(unsigned int i=0;i<*maxClients;i++)
		{
			player = players.at(i);

			if(*player->GetConnState() == 0){ continue; } // If the player is not connected

			searchIp = player->GetIPAdr();

			if(searchIp.find(partialIp, 0) != -1)
			{
				arr.Append(players.at(i));
			}
		}
		return arr;
	}*/
	
	
	
// ** 3. LIMBO FUNCTIONS *************************************************//
// =======================================================================//

	/**
	 * Accepts an incoming IP address
	 * @param ip
	 * @param enforce
	 */
	void Server::ASLimboAccept(char* ip, bool enforce)
	{
		bool limboFound = false;
		unsigned int limboSize = limboConnections.size();

		/* Loop through all the limbo connections and once an IP adr match
		   is found then flag it as accepted */
		for(unsigned int i=0;i<limboSize;i++)
		{
			if(strcmp(limboConnections.at(i).ip, ip) == 0)
			{
				limboConnections.at(i).state = 1;
				limboFound = true;

				break;
			}
		}

		// If the coder requires that the IP needs to be appended to the list
		if(!limboFound && enforce)
		{
			limboConnections.push_back(Limbo());
			limboSize = limboConnections.size();
			limboConnections.at(limboSize - 1).ip = ip;

			printf("IP added to limbo queue: %s\n", limboConnections.at(limboSize - 1).ip);
		}
	}

	/**
	 * Denies an incoming join request, displays reason on player's screen when he's rejected
     * @param ip
     * @param reason
     * @param enforce
     */
	void Server::ASLimboDeny(char* ip, char* reason, bool enforce)
	{
		bool limboFound = false;
		unsigned int limboSize = limboConnections.size();

		/* Loop through all the limbo connections and once an IP adr match
		   is found then flag it as denied */
		for(unsigned int i=0;i<limboSize;i++)
		{
			if(strcmp(limboConnections.at(i).ip, ip) == 0)
			{
				limboConnections.at(i).state = 2;
				limboConnections.at(i).denyReason = reason;
				limboFound = true;

				break;
			}
		}

		// If the coder requires that the IP needs to be appended to the list
		if(!limboFound && enforce)
		{
			limboConnections.push_back(Limbo());
			limboSize = limboConnections.size();
			limboConnections.at(limboSize - 1).ip = ip;

			printf("IP added to limbo queue: %s\n", limboConnections.at(limboSize - 1).ip);
		}
	}
	
	

// ** 4. GROUP FUNCTIONS *************************************************//
// =======================================================================//
	
	void Server::ASDelegateGroups()
	{
		Server::database->AllocateGroups(&Server::groups);
	}


	
// ** 5. GAMEPLAY FUNCTIONS **********************************************//
// =======================================================================//
	
	void Server::ASLoadNextMap()
	{
		Server::ExecCmd("map_rotate");
	}
	
	void Server::ASRestartMap()
	{
		Server::ExecCmd("map_restart");
	}
	
	void Server::ASResetMap()
	{
		Server::ExecCmd("fast_restart");
	}
	
	void Server::ASLoadMap(std::string mapName)
	{
		Server::ExecCmd(utils::string_format("map %s", mapName.c_str()));
	}
	
	
	
// ** 6. VIOLATION FUNCTIONS *********************************************//
// =======================================================================//
	
	/*Ban Server::ASGetBanByIp(char* ip)
	{
		return Server::database->GetBanByIp(ip);
	}*/