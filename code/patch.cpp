#include "patch.h"

void Patch::Byte(void* address, int byte)
{
}

void Patch::Blockout(void* startAddr, unsigned long length)
{
	// Initialize our page size variables
    mPageSize = sysconf(_SC_PAGE_SIZE);
    mPageMask = (~(mPageSize - 1));
	
//	mprotect(GetPage((unsigned long) (targetFunction)),mPageSize,PROT_READ|PROT_WRITE|PROT_EXEC);
    memset(startAddr, 0x00, length);
}