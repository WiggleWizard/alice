/* 
 * File:   MongoDB.h
 * Author: zinglish
 *
 * Created on 11 November 2013, 00:19
 */

#ifndef MONGODB_H
#define	MONGODB_H

#define MONGO_USE_LONG_LONG_INT

extern "C" {
	#include "mongo.h"
}
#include "../player.h"
#include "../objects/community.h"
#include "../objects/ban.h"

class MongoDB
{
	public:
		// ctor
		MongoDB();

		bool Connect(char* address, int port);
		
		//
		bool PlayerLogin(char* username, char* password, Player* player);
		
		Community GetCommunity(char* communityRef);
		Ban* GetIpBan(char* community, char* address);
		
		// Debug printing functions
		void PrintFatal(char* message);
		void PrintDebug(char* message);
		
		// dtor
		virtual ~MongoDB();
	private:
		mongo connection;
};

#endif	/* MONGODB_H */

