/* 
 * File:   MongoDB.cpp
 * Author: zinglish
 * 
 * Created on 11 November 2013, 00:19
 */

#include "mongodb.h"
#include <algorithm>

MongoDB::MongoDB()
{
	mongo_init(&connection);
	mongo_set_op_timeout(&connection, 1000);
}

/**
 * Connect to the specified database
 * @param address
 * @param port
 * @return True if connection was successful
 */
bool MongoDB::Connect(char* address, int port)
{
	int status = mongo_client(&connection, address, port);
	
	if(status != MONGO_OK)
	{
		switch (connection.err)
		{
			case MONGO_CONN_NO_SOCKET:
				PrintFatal("Socket not available");
				return false;
			case MONGO_CONN_FAIL:
				PrintFatal("Connection failed");
				return false;
			case MONGO_CONN_NOT_MASTER:
				PrintFatal("Not master");
				return false;
		}
	}
	
	return true;
}


/**
 * 
 * 
 * 
 */

/**
 * Authenticates a player and appends some details about his auth session
 * to the player object
 * @param username
 * @param password
 * @param player
 * @return 
 */
bool MongoDB::PlayerLogin(char* username, char* password, Player* player)
{
	bool successful = false;
	bson query[1];
	mongo_cursor cursor[1];
	
	bson_init(query);
	bson_append_string(query, "password", password);
	bson_append_string(query, "username", username);
	bson_finish(query);
	
	mongo_cursor_init(cursor, &connection, "metro.users");
	mongo_cursor_set_query(cursor, query);
	
	while(mongo_cursor_next(cursor) == MONGO_OK)
	{
		bson_iterator i[1];

		// Append the id of the player's auth
		if(bson_find(i, mongo_cursor_bson(cursor), "_id"))
		{
//			bson_oid_to_string(bson_iterator_oid(i), player->authId);
//			printf("Login success: %s\n", player->authId);
			successful = true;
		}
		
		// Append any communities the player is in
		if(bson_find(i, mongo_cursor_bson(cursor), "communities"))
		{
			bson_iterator community_i[1];
			bson_iterator_subiterator(i, community_i);
			
//			while(bson_iterator_next(community_i))
//				player->communities.push_back(bson_iterator_string(community_i));
		}
	}
	
	bson_destroy(query);
	mongo_cursor_destroy(cursor);
	
	return successful;
}

/**
 * Finds a ban from a community by IP
 * @param community
 * @param address
 * @return 
 */
Ban* MongoDB::GetIpBan(char* community, char* address)
{
	// Sanitize the address so we can do a search
	std::string addr = address;
	std::replace(addr.begin(), addr.end(), '.', '/');
	
	Ban* ban = NULL;
	bson query[1];
	mongo_cursor cursor[1];
	
	bson_init(query);
	bson_append_string(query, "_id", community);
	bson_finish(query);
	
	mongo_cursor_init(cursor, &connection, "metro.communities");
	mongo_cursor_set_query(cursor, query);
	
	while(mongo_cursor_next(cursor) == MONGO_OK)
	{
		bson_iterator i[1];
		
		if(bson_find(i, mongo_cursor_bson(cursor), "ipBans"))
		{
			bson_iterator search_i[1];
			bson search_query[1];
			
			bson_iterator_subobject_init(i, search_query, false);
			
			// The IP was found
			if(bson_find(search_i, search_query, addr.c_str()))
			{
				ban = new Ban();
				bson_iterator ban_i[1];
			
				bson_iterator_subiterator(search_i, ban_i);
				
				while(bson_iterator_next(ban_i))
				{
					if(strcmp(bson_iterator_key(ban_i), "playerName") == 0)
						ban->playerName = bson_iterator_string(ban_i);
					else if(strcmp(bson_iterator_key(ban_i), "playerGuid") == 0)
						ban->playerGuid = bson_iterator_string(ban_i);
					else if(strcmp(bson_iterator_key(ban_i), "reason") == 0)
						ban->reason = bson_iterator_string(ban_i);
				}
			}
			
			break;
		}
	}
	
	bson_destroy(query);
	mongo_cursor_destroy(cursor);
	
	return ban;
}


/**
 * 
 * D E B U G  P R I N T I N G  F U N C T I O N S
 * 
 */

void MongoDB::PrintFatal(char* message)
{
	printf("MongoDB | FATAL | %s\n", message);
}
void MongoDB::PrintDebug(char* message)
{
	printf("MongoDB | DEBUG | %s\n", message);
}


// dtor
MongoDB::~MongoDB()
{
	mongo_destroy(&connection);
}

