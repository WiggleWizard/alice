/* 
 * File:   utils.h
 * Author: zinglish
 *
 * Created on 11 November 2013, 03:40
 */

#ifndef UTILS_H
#define	UTILS_H

#include <vector>
#include <string>

class utils {
	public:
		static std::vector<char*> strxpld(char* s, char* delim);
		static std::vector<char*> strxpld(char* s, char* delim, unsigned int limit);
		static std::string string_format(const std::string fmt_str, ...);
	private:

};

#endif	/* UTILS_H */

