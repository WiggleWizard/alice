#ifndef PATCH_H
#define PATCH_H

#include <stdio.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

class Patch
{
    public:
        void Byte(void* address, int byte);
		void Blockout(void* startAddr, unsigned long length);
    protected:
    private:
        unsigned long mPageSize;
        unsigned long mPageMask;
};

#endif // PATCH_H
