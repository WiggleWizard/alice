/* 
 * File:   warning.h
 * Author: zinglish
 *
 * Created on 11 November 2013, 14:41
 */

#ifndef WARNING_H
#define	WARNING_H

class Warning {
public:
	Warning();
	Warning(const Warning& orig);
	virtual ~Warning();
private:

};

#endif	/* WARNING_H */

