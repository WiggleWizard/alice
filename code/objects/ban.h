/* 
 * File:   ban.h
 * Author: zinglish
 *
 * Created on 11 November 2013, 18:35
 */

#ifndef BAN_H
#define	BAN_H

#include <string>
#include "time.h"

using std::string;

class Ban {
public:
	Ban();
	virtual ~Ban();
	
	// AliceScript Data functions
	unsigned int ASGetId() { return this->id; };
	string       ASGetPlayerIp() { return this->playerIp; };
	string       ASGetPlayerName() { return this->playerName; };
	string       ASGetPlayerGuid() { return this->playerGuid; };
	string       ASGetAdminName() { return this->adminName; };
	string       ASGetReason() { return this->reason; };
	
	// Variables
	unsigned int id;
	string       playerIp;
	string       playerName;
	string       playerGuid;
	u_int32_t    adminId;
	string       adminName;
	string       reason;
	Time*        time;
	u_int32_t    type;
	Time*        unbanTime;
private:

};

#endif	/* BAN_H */

