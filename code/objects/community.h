/* 
 * File:   community.h
 * Author: zinglish
 *
 * Created on 11 November 2013, 16:51
 */

#ifndef COMMUNITY_H
#define	COMMUNITY_H

class Community {
public:
	Community(char* communityRef);

	// Variables
	char* ref;
	char* name;
	
	virtual ~Community();
private:

};

#endif	/* COMMUNITY_H */

