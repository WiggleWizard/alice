/* 
 * File:   time.cpp
 * Author: zinglish
 * 
 * Created on 24 February 2014, 21:47
 */

#include "time.h"

/**
 * Initializes with local current time
 */
Time::Time()
{
	this->pt = second_clock::local_time();
	this->td = this->pt.time_of_day();
}

/**
 * @param time String formatted like "2002-01-20 23:59:59.000"
 */
Time::Time(string time)
{
	this->pt = ptime(time_from_string(time));
	this->td = this->pt.time_of_day();
}

Time::Time(const Time& orig) {
}

Time::~Time() {}

void Time::operator +(Time* time)
{
	1 + 1;
}

string Time::Format(string format)
{
	std::string s;
	std::ostringstream ss;
	time_facet* facet = new time_facet(format.c_str());
	ss.imbue(std::locale(ss.getloc(), facet));
	ss << this->pt;
	s = ss.str();

	return s;
}