/* 
 * File:   time.h
 * Author: zinglish
 *
 * Created on 24 February 2014, 21:47
 */

#ifndef TIME_H
#define	TIME_H

#include <string>
#include "boost/date_time/posix_time/posix_time.hpp"

using namespace boost::posix_time;
using std::string;

class Time {
public:
	Time();
	Time(string time);
	Time(const Time& orig);
	virtual ~Time();
	
	// Calculations
	void operator +(Time* time);
	
	// Output
	string Format(string format);
	
	
private:
	ptime          pt;
	time_duration  td;
};

#endif	/* TIME_H */

