#ifndef PLAYERGROUP_H
#define	PLAYERGROUP_H

#include <string>
#include <vector>

class PlayerGroup {
public:
	// ctor and dtor
	PlayerGroup(unsigned int id, unsigned int rank, std::string name);
	virtual ~PlayerGroup();
	
	// Variables
	unsigned int id;
	unsigned int rank;
	std::string name;
	std::vector<std::string> perms;
	
	// AliceScript Functions
//	Sqrat::string ASGetName();
	unsigned int ASGetRank();
//	bool ASHasPermKey(Sqrat::string permKey);
};

#endif	/* SIGILGROUP_H */

