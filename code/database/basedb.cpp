/* 
 * File:   database.cpp
 * Author: zinglish
 * 
 * Created on 12 November 2013, 18:42
 */

#include "database.h"
#include "../player.h"
#include "../utils.h"
#include "../server.h"
#include "../objects/playergroup.h"

#include <stdio.h>
#include <unistd.h>
#include <string.h> /* for strncpy */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

Database::Database()
{
	this->driver = get_driver_instance();
}

/**
 * Attempts to connect to the database
 * @param hostname
 * @param username
 * @param password
 * @param database
 * @return False if failed
 */
bool Database::Connect(char* hostname, char* username, char* password, char* database)
{
	bool success = false;
	try
	{
		this->conn = this->driver->connect(hostname, username, password);
		this->conn->setSchema(database);
		
		success = true;
	}
	catch (sql::SQLException &e) {}
	
	return success;
}

// Functions

/**
 * Sets server up with group information and community ID.
 * As well as executing the config users write and adding AliceScripts into memory
 * @return 
 */
bool Database::SetupServer()
{
	/* Get the IP of the assigned interface */
	int fd;
	struct ifreq ifr;
	std::string ipAddr;
	
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	ifr.ifr_addr.sa_family = AF_INET; // IPv4
	strncpy(ifr.ifr_name, "lo", IFNAMSIZ-1); // IP address attached to eth0
	ioctl(fd, SIOCGIFADDR, &ifr);
	close(fd);

	ipAddr = inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
	Server::PrintDebug(utils::string_format("Searching for server ID %i to run on machine IP %s", Server::serverId, ipAddr.c_str()));
	
	/*  */
	unsigned int communityId = 0;
	unsigned int serverId = 0;
	
	std::auto_ptr<PreparedStatement> pstmt(this->conn->prepareStatement(
		" SELECT"
			" community_id,"
			" servers.id AS server_id,"
			" servers.name AS server_name,"
			" servers.port AS server_port,"
			" servers.max_clients AS server_clients"
		" FROM"
			" servers"
		" JOIN"
			" machines ON (servers.machine_id=machines.id)"
		" WHERE"
			" machines.ip=? AND servers.id=?"
	));
	pstmt->setString(1, ipAddr);
	pstmt->setInt(2, Server::serverId);
	std::auto_ptr<ResultSet> resultSet(pstmt->executeQuery());
	
	// Start the server if the server ID and machine IP are a match
	if(resultSet->next())
	{
		serverId = resultSet->getInt("server_id");
		communityId = resultSet->getInt("community_id");

		Server::PrintDebug(utils::string_format("Found community ID: %i, now executing server ID %i", communityId, serverId));
		
		Server::communityId = communityId; // Assign community ID
		Server::cfgMaxClients = resultSet->getInt("server_clients"); // Max amount of players
		Server::cfgServerName = new char[128];
		strcpy(Server::cfgServerName, resultSet->getString("server_name").c_str()); // Server name
		Server::cfgPort = resultSet->getInt("server_port");
	}
	else
	{
		Server::PrintDebug(utils::string_format("Refusing server ID %i to be started on this machine because server ID is not allowed on this machine IP, aborting\n", Server::serverId));
		exit(0);
	}
	
	if(communityId > 0)
		return true;
	else
		return false;
}

bool Database::LogPlayerIn(std::string username, std::string password, Player* player)
{
	bool success = false;
	
	// Get the data from the database
	std::auto_ptr<PreparedStatement> pstmt(this->conn->prepareStatement(
		" SELECT"
			" users.id AS id,"
			" community_id AS community,"
			" group_id AS `group`"
		" FROM"
			" users"
		" LEFT JOIN"
			" users_communities ON (users.id=users_communities.user_id)"
		" WHERE"
			" username=? AND"
			" ingame_password=?"
	));
	pstmt->setString(1, username);
	pstmt->setString(2, password);
	std::auto_ptr<ResultSet> resultSet(pstmt->executeQuery());
	
	// Found result
	while(resultSet->next())
	{
		success = true;
		player->loggedIn = true;
		player->sigilId = resultSet->getInt("id");
		player->sigilUsername = username;

		// Get the player's community, if the player is in this community then assign him his group
		const char* sqlString = resultSet->getString("community").c_str();
		unsigned int comID = atoi(sqlString);
		
		player->communities.push_back(comID);
		if(comID == Server::communityId)
		{
			sqlString = resultSet->getString("group").c_str();
			
			// If the group doesn't exist or has not been delegated then throw an error
			if(Server::GetGroupById(atoi(sqlString)) == NULL)
				Server::PrintFatal("Group not found when trying to log player in, player's group id will remain 1 (Guest)\n");
			else
				player->group = Server::GetGroupById(atoi(sqlString));
		}
	}
	
	return success;
}

/**
 * Purges the vector, then constructs a new one with details from the
 * database the class is connected to
 * @param Pointer to a vector of playergroup pointers
 */
void Database::AllocateGroups(std::vector<PlayerGroup*>* groups)
{
	// Purge the vector
	for(unsigned int i=0;i<groups->size();i++)
		delete groups->at(i);
	if(groups->size() > 0)
	{
		this->PrintDebug("Deallocating groups");
		groups->clear();
		groups->reserve(10); // Average sized community will have less than 10 groups
	}

	// Build the groups vector
	this->PrintDebug("Allocating groups");
	try
	{
		std::string dbQuery = utils::string_format(
			" SELECT "
				" %d_groups.id AS id,"
				" %d_groups.name AS group_name,"
				" rank,"
				" `key` AS perm_key"
			" FROM"
				" %d_groups"
			" LEFT JOIN"
				" %d_alice_perms ON (%d_alice_perms.`group`=%d_groups.id)"
			" LEFT JOIN"
				" %d_dev_perms ON (%d_dev_perms.id = %d_alice_perms.dev_perm)",
		Server::communityId, Server::communityId, Server::communityId, 
				Server::communityId, Server::communityId, Server::communityId, 
				Server::communityId, Server::communityId, Server::communityId);
		std::auto_ptr<Statement> stmt(conn->createStatement());
		std::auto_ptr<ResultSet> resultSet(stmt->executeQuery(dbQuery));

		while(resultSet->next())
		{
			bool found = false;
			unsigned int groupId = resultSet->getInt("id");
			std::string permKey = resultSet->getString("perm_key");
			
			// Checks if this group has been created before, if so then add the permission key to the set
			for(unsigned int i=0;i<groups->size();i++)
			{
				if(groups->at(i)->id == groupId)
				{
					if(permKey != "")
					{
						this->PrintDebug(utils::string_format("Assigning group '%s' key: %s", resultSet->getString("group_name").c_str(), permKey.c_str()));

						PlayerGroup* group = groups->at(i);
						group->perms.push_back(permKey);
					}
					
					found = true;
				}
			}
			
			// If no group was found to insert the key in, then make the group and insert the key found, if there is one
			if(!found)
			{
				this->PrintDebug(utils::string_format("Creating group: %s", resultSet->getString("group_name").c_str()));
				
				// Make sure we are adding key to the right group
				groups->push_back(new PlayerGroup(
					groupId,
					resultSet->getInt("rank"),
					resultSet->getString("group_name")
				));
				
				std::string permKey = resultSet->getString("perm_key");
				if(permKey != "")
				{
					this->PrintDebug(utils::string_format("Assigning group '%s' key: %s", resultSet->getString("group_name").c_str(), permKey.c_str()));
					
					PlayerGroup* group = groups->at(groups->size() - 1);
					group->perms.push_back(permKey);
				}
			}
		}
		
		this->PrintDebug("Group allocations completed successfully");
	}
	catch (sql::SQLException &e)
	{
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
}

/**
 * Sets the server's config to execute at initialization time
 * @return 
 */
std::string Database::GetConfig()
{
	std::string rtn = "";
	
	// Get the data from the database
	std::string dbQuery = utils::string_format(
		" SELECT"
			" %d_configs.name AS config_name,"
			" config"
		" FROM"
			" %d_configs"
		" LEFT JOIN"
			" servers ON (servers.config_id=%d_configs.id)"
		" WHERE"
			" servers.config_id=?",
		Server::communityId, Server::communityId, Server::communityId);
	std::auto_ptr<PreparedStatement> pstmt(this->conn->prepareStatement(dbQuery));
	pstmt->setInt(1, Server::communityId);
	std::auto_ptr<ResultSet> resultSet(pstmt->executeQuery());
	
	// Found result
	while(resultSet->next())
	{
		rtn = resultSet->getString("config");
		break;
	}
	
	return rtn;
}
	

/**
 * 
 * D E B U G  P R I N T I N G  F U N C T I O N S
 * 
 */

void Database::PrintFatal(char* message)
{
	printf("Database | FATAL | %s\n", message);
	exit(0);
}
void Database::PrintDebug(std::string message)
{
	printf("Database | DEBUG | %s\n", message.c_str());
}


Database::~Database() {
}

