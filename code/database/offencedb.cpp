#include "database.h"

#include "../server.h"
#include "../objects/ban.h"
#include "../objects/time.h"
#include "../utils.h"

// ******************************* BANS **********************************//
// ***********************************************************************//

	/**
	 * Collates the banned IP's details into a ban object
	 * @param ip
	 * @return 
	 */
	Ban Database::GetBanByIp(char* ip)
	{
		Ban ban = Ban();

		// Get the data from the database
		std::string dbQuery = utils::string_format(
				"SELECT "
					"ban.id             AS id,"
					"ban.player_ip      AS player_ip,"
					"ban.player_name    AS player_name,"
					"ban.player_guid    AS player_guid,"
					"users.id           AS admin_id,"
					"users.display_name AS admin_name,"
					"ban.reason         AS reason,"
					"ban.timestamp      AS time "
				"FROM %d_bans AS ban "
				"JOIN users ON (ban.admin_sigil_id=users.id) "
				"WHERE player_ip=? AND banned=1 "
				"LIMIT 1",
			Server::communityId);
		std::auto_ptr<PreparedStatement> pstmt(this->conn->prepareStatement(dbQuery));
		pstmt->setString(1, ip);
		std::auto_ptr<ResultSet> resultSet(pstmt->executeQuery());

		// Set the ban object variables
		while(resultSet->next())
		{
			ban.id         = resultSet->getUInt("id");
			ban.playerIp   = resultSet->getString("player_ip");
			ban.playerName = resultSet->getString("player_name");
			ban.playerGuid = resultSet->getString("player_guid");
			ban.adminId    = resultSet->getUInt("admin_id");
			ban.adminName  = resultSet->getString("admin_name");
			ban.reason     = resultSet->getString("reason");
			ban.time       = new Time(resultSet->getString("time"));

			break;
		}

		return ban;
	}

	/**
	 * Issues a ban using a ban object
	 * @param ban
	 * @return 
	 */
	bool Database::IssueBan(Ban ban)
	{
		// Collate the ban data into the database
		std::string dbQuery = utils::string_format(
				"INSERT INTO %d_bans "
					"(player_name, player_ip, player_guid,"
					"admin_sigil_id, admin_ign, reason, timestamp)"
				"VALUES "
					"(?, ?, ?,"
					"?, ?, ?, ?)",
			Server::communityId);
		std::auto_ptr<PreparedStatement> pstmt(this->conn->prepareStatement(dbQuery));
		pstmt->setString(1, ban.playerName);
		pstmt->setString(2, ban.playerIp);
		pstmt->setString(3, ban.playerGuid);
		pstmt->setUInt(4, ban.adminId);
		pstmt->setString(5, ban.adminName);
		pstmt->setString(6, ban.reason);
		pstmt->setString(7, ban.time->Format("%Y-%m-%d %H:%M:%S")); // http://www.boost.org/doc/libs/1_35_0/doc/html/date_time/date_time_io.html#date%5Ftime.format%5Fflags

		pstmt->executeQuery();

		return true;
	}

// ***************************** WARNINGS ********************************//
// ***********************************************************************//