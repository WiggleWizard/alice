/* 
 * File:   database.h
 * Author: zinglish
 *
 * Created on 12 November 2013, 18:42
 */

#ifndef DATABASE_H
#define	DATABASE_H

#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

using std::string;

// Fake classes
class Player;
class PlayerGroup;
class Ban;

using namespace sql;

class Database {
	public:
		Database();
		virtual ~Database();
		
		// Core functions
		bool Connect(char* hostname, char* username, char* password, char* database);

		// Functions
		bool SetupServer();
		bool LogPlayerIn(std::string username, std::string password, Player* player);
		void AllocateGroups(std::vector<PlayerGroup*>* groups);
		std::string GetConfig();
		
		// Offence Data
		Ban  GetBanByIp(char* ip);
		bool IssueBan(Ban ban);
		
		// Local Variables
		Driver* driver;
		Connection* conn;

		// Debug printing functions
		void PrintFatal(char* message);
		void PrintDebug(std::string message);
	private:

};

#endif	/* DATABASE_H */

