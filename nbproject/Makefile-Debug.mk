#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/code/asengine/engine.o \
	${OBJECTDIR}/code/asengine/wrapPlayer.o \
	${OBJECTDIR}/code/database/basedb.o \
	${OBJECTDIR}/code/database/offencedb.o \
	${OBJECTDIR}/code/hook.o \
	${OBJECTDIR}/code/main.o \
	${OBJECTDIR}/code/objects/ban.o \
	${OBJECTDIR}/code/objects/community.o \
	${OBJECTDIR}/code/objects/playergroup.o \
	${OBJECTDIR}/code/objects/playerwarn.o \
	${OBJECTDIR}/code/objects/time.o \
	${OBJECTDIR}/code/patch.o \
	${OBJECTDIR}/code/player.o \
	${OBJECTDIR}/code/server/asfunctions.o \
	${OBJECTDIR}/code/server/base.o \
	${OBJECTDIR}/code/server/common.o \
	${OBJECTDIR}/code/server/hookedfunctions.o \
	${OBJECTDIR}/code/utils.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-m32 -Wall -fexceptions -fPIC -O0 -J3
CXXFLAGS=-m32 -Wall -fexceptions -fPIC -O0 -J3

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-Llibs/mongo-c-driver/build -Llibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/lib -Llibs/boost_1_55_0/build/lib -Llibs/v8/out/ia32.debug/obj.target/tools/gyp -Llibs/v8/out/ia32.debug/obj.target/third_party/icu -lbson -lmongoc -lmysqlcppconn -lboost_filesystem -lboost_program_options -lboost_system -lboost_thread -lv8_base.ia32 -licui18n -licuuc -licudata -lv8_snapshot

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libAlice.${CND_DLIB_EXT}

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libAlice.${CND_DLIB_EXT}: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	g++ -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libAlice.${CND_DLIB_EXT} ${OBJECTFILES} ${LDLIBSOPTIONS} -ldl -m32 -fPIC -shared -fPIC

${OBJECTDIR}/code/asengine/engine.o: code/asengine/engine.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/asengine
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/asengine/engine.o code/asengine/engine.cpp

${OBJECTDIR}/code/asengine/wrapPlayer.o: code/asengine/wrapPlayer.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/asengine
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/asengine/wrapPlayer.o code/asengine/wrapPlayer.cpp

${OBJECTDIR}/code/database/basedb.o: code/database/basedb.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/database
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/database/basedb.o code/database/basedb.cpp

${OBJECTDIR}/code/database/offencedb.o: code/database/offencedb.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/database
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/database/offencedb.o code/database/offencedb.cpp

${OBJECTDIR}/code/hook.o: code/hook.cpp 
	${MKDIR} -p ${OBJECTDIR}/code
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/hook.o code/hook.cpp

${OBJECTDIR}/code/main.o: code/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/code
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/main.o code/main.cpp

${OBJECTDIR}/code/objects/ban.o: code/objects/ban.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/objects
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/objects/ban.o code/objects/ban.cpp

${OBJECTDIR}/code/objects/community.o: code/objects/community.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/objects
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/objects/community.o code/objects/community.cpp

${OBJECTDIR}/code/objects/playergroup.o: code/objects/playergroup.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/objects
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/objects/playergroup.o code/objects/playergroup.cpp

${OBJECTDIR}/code/objects/playerwarn.o: code/objects/playerwarn.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/objects
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/objects/playerwarn.o code/objects/playerwarn.cpp

${OBJECTDIR}/code/objects/time.o: code/objects/time.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/objects
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/objects/time.o code/objects/time.cpp

${OBJECTDIR}/code/patch.o: code/patch.cpp 
	${MKDIR} -p ${OBJECTDIR}/code
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/patch.o code/patch.cpp

${OBJECTDIR}/code/player.o: code/player.cpp 
	${MKDIR} -p ${OBJECTDIR}/code
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/player.o code/player.cpp

${OBJECTDIR}/code/server/asfunctions.o: code/server/asfunctions.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/server
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/server/asfunctions.o code/server/asfunctions.cpp

${OBJECTDIR}/code/server/base.o: code/server/base.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/server
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/server/base.o code/server/base.cpp

${OBJECTDIR}/code/server/common.o: code/server/common.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/server
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/server/common.o code/server/common.cpp

${OBJECTDIR}/code/server/hookedfunctions.o: code/server/hookedfunctions.cpp 
	${MKDIR} -p ${OBJECTDIR}/code/server
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/server/hookedfunctions.o code/server/hookedfunctions.cpp

${OBJECTDIR}/code/utils.o: code/utils.cpp 
	${MKDIR} -p ${OBJECTDIR}/code
	${RM} "$@.d"
	$(COMPILE.cc) -g -w -Ilibs/mongo-c-driver/build/include -Ilibs/mysql-connector-c++-1.1.3-linux-glibc2.3-x86-32bit/include -Ilibs/boost_1_55_0/build/include -Ilibs/v8/include -std=c++11 -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/code/utils.o code/utils.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libAlice.${CND_DLIB_EXT}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
